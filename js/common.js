/*
 * Calendar reload
 */

$(document).on("click", ".caption_nav_prev a", function ()
{
    var date = $(this).attr('id').substr(2);
    $('#mod_calendar2 .module_calendar').load('index.php?c=get_calendar&date=' + date);

    $('#calendarNews').load('index.php?c=get_calendar&date=' + date);
});

$(document).on("click", ".caption_nav_next a", function ()
{
    var date = $(this).attr('id').substr(2);
    $('#mod_calendar2 .module_calendar').load('index.php?c=get_calendar&date=' + date);

    $('#calendarNews').load('index.php?c=get_calendar&date=' + date);
});

$(document).ready(function () {
    
    var isMobileView = false;
    var isLargeView = false;
    var isMediumView = false;
    var isModuleHeightAlign = false;

    $('.clock-container').append('<svg class="clock-container__background" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="76px" height="77px"><defs><filter filterUnits="userSpaceOnUse" id="clock-container-filter" x="0px" y="0px" width="76px" height="77px"><feOffset in="SourceAlpha" dx="0" dy="1" /><feGaussianBlur result="blurOut" stdDeviation="0" /><feFlood flood-color="rgb(180, 194, 24)" result="floodOut" /><feComposite operator="atop" in="floodOut" in2="blurOut" /><feComponentTransfer><feFuncA type="linear" slope="1"/></feComponentTransfer><feMerge><feMergeNode/><feMergeNode in="SourceGraphic"/></feMerge></filter></defs><g filter="clock-container-filter"><path fill-rule="evenodd" d="M37.941,0.618 C58.619,0.618 75.382,17.381 75.382,38.059 C75.382,58.737 58.619,75.500 37.941,75.500 C17.263,75.500 0.500,58.737 0.500,38.059 C0.500,17.381 17.263,0.618 37.941,0.618 Z"/></g></svg>');

    if (!$('#modules-bottom li').length) {
        $('.main').addClass('desktop-padding');
    }

    if ($('.sidebar-modules > div').length == 1) {
        $('.sidebar-modules > div').addClass('sm-offset-center');
    }

    if (!$('.navbar .topMenu').length) {
        $('.navbar').hide();
    } 

    $('.footer__gotop').on('click', function(e) {
        e.preventDefault();
        $('html,body').animate({ scrollTop: 0 }, 'slow');
        $('.page').focus();
        return false; 
    });

    if (!$('.sidebar-menu ul').length > 0) {
        $('.sidebar-menu').hide();
    }
    
    $('#btnFilePos').on('click', function() {
        $('#avatar_f').trigger('click');
    });

    $('.menu-top .dropdown-submenu a.selected').closest('.dropdown-submenu').children('a').addClass('selected');

    if (isMobile.any) {
        $('body').addClass('is-mobile');

        $('.dropdown-submenu', '#navbar-top').on('hide.bs.dropdown', function () {
            return false;
        });
    }

    $(".board table, .main-text table").each(function() {
        var $table = $(this);
        
        if ($table.parent('.table-responsive').length === 0) {
            $table.wrap('<div class="table-responsive"></div>');
        }
    });

    $('.modules-content > div').each(function() {
        $(this).on('click', function() {
            if ($(this).find('a').length > 0) {
                var href = $(this).find('a').attr('href');
                window.location.href = href;
            }
            if ($(this).is('#mod_contact')) {
                window.location.href = 'kontakt';
            }
        });
    });

    $('.protectedPage').each(function(index) {
        $('<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 7 10"><defs><filter filterUnits="userSpaceOnUse" id="filter-lock-' + index + '" x="0px" y="0px" width="7px" height="10px"><feOffset in="SourceAlpha" dx="0" dy="1" /><feGaussianBlur result="blurOut" stdDeviation="0" /><feFlood flood-color="rgb(255, 189, 0)" result="floodOut" /><feComposite operator="atop" in="floodOut" in2="blurOut" /><feComponentTransfer><feFuncA type="linear" slope="1"/></feComponentTransfer><feMerge><feMergeNode/><feMergeNode in="SourceGraphic"/></feMerge></filter></defs><g filter="url(#filter-lock- ' + index + ')"><path fill-rule="evenodd" d="M6.124,9.000 L0.867,9.000 C0.383,9.000 -0.010,8.622 -0.010,8.156 L-0.010,4.781 C-0.010,4.315 0.383,3.937 0.867,3.937 L1.159,3.937 L1.159,2.250 C1.159,1.007 2.205,-0.000 3.495,-0.000 C4.786,-0.000 5.832,1.007 5.832,2.250 L5.832,3.920 L4.663,3.920 L4.663,1.969 C4.663,1.503 4.140,1.125 3.495,1.125 C2.850,1.125 2.327,1.503 2.327,1.969 L2.327,3.937 L6.124,3.937 C6.608,3.937 7.000,4.315 7.000,4.781 L7.000,8.156 C7.000,8.622 6.608,9.000 6.124,9.000 ZM3.495,5.062 C2.850,5.062 2.327,5.566 2.327,6.187 C2.327,6.599 2.568,6.944 2.911,7.140 L2.911,7.857 C2.911,8.168 3.173,8.420 3.495,8.420 C3.818,8.420 4.079,8.168 4.079,7.857 L4.079,7.140 C4.422,6.944 4.663,6.599 4.663,6.187 C4.663,5.566 4.140,5.062 3.495,5.062 Z"/></g></svg>').appendTo(this);
    });

    $(".owl-carousel").owlCarousel({
        singleItem: true,
        autoPlay: 1e3 * settings.duration,
        slideSpeed: 1e3 * settings.animationDuration,
        paginationSpeed: 1e3 * settings.animationDuration,
        transitionStyle: settings.transition,
        pagination: false,
        mouseDrag: false,
        touchDrag: false
    });

    function equalizeHeights(selector) {
        var heights = new Array();
        $(selector).each(function() {
            $(this).css('min-height', '0');
            $(this).css('max-height', 'none');
            $(this).css('height', 'auto');
            heights.push($(this).height());
        });

        var max = Math.max.apply(Math, heights);

        $(selector).each(function() {
            $(this).css('height', max + 'px');
        }); 
    }

    function equalizeHeightsWithPadding(selector) {
        var heights = new Array();
        $(selector).each(function() {
            $(this).css('min-height', '0');
            $(this).css('max-height', 'none');
            $(this).css('height', 'auto');
            heights.push($(this).outerHeight());
        });

        var max = Math.max.apply( Math, heights );

        $(selector).each(function() {
            $(this).css('height', max + 'px');
        }); 
    }

    function fancyboxInit() {
        var a;
        $("a.fancybox").fancybox({
            overlayOpacity: .9,
            overlayColor: settings.overlayColor,
            titlePosition: "outside",
            titleFromAlt: !0,
            titleFormat: function (a, b, c, d) {
                return '<span id="fancybox-title-over">' + texts.image + " " + (c + 1) + " / " + b.length + "</span>" + (a.length ? " &nbsp; " + a.replace(texts.enlargeImage + ": ", "") : "")
            },
            onStart: function (b, c, d) {
                a = b[c];
            },
            onComplete: function () {
            },
            onClosed: function () {
            }
        });
    }

    fancyboxInit();

    function watch() {
        var s = Snap(document.getElementById("clock"));
        var seconds = s.select("#handSecond"),
            minutes = s.select("#handMinute"),
            hours = s.select("#handHour"),
            face = {
                elem: s.select("#face"),
                cx: s.select("#face").getBBox().cx,
                cy: s.select("#face").getBBox().cy
            },
            angle = 0;
        function update() {
            var time = new Date();
            setHours(time);
            setMinutes(time);
            setSeconds(time);
        }
        function setHours(t) {
            var hour = t.getHours();
            hour %= 12;
            hour += Math.floor(t.getMinutes()/10)/6;
            var angle = hour*360/12;
            hours.animate(
                {transform: "rotate("+angle+" "+face.cx+" "+face.cy+")"},
                100,
                mina.linear,
                function(){
                    if (angle === 360) {
                        hours.attr({transform: "rotate("+0+" "+face.cx+" "+face.cy+")"});               
                    }
                }
              );
        }
        function setMinutes(t) {
            var minute = t.getMinutes();
            minute %= 60;
            minute += Math.floor(t.getSeconds()/10)/6;
            var angle = minute*360/60;
            minutes.animate(
                {transform: "rotate("+angle+" "+face.cx+" "+face.cy+")"},
                100,
                mina.linear,
                function() {
                    if (angle === 360) {
                        minutes.attr({transform: "rotate("+0+" "+face.cx+" "+face.cy+")"});
                    }
                }
            );
        }
        function setSeconds(t) {
            t = t.getSeconds();
            t %= 60;
            var angle = t*360/60;
            if (angle === 0) angle = 360;
            seconds.animate(
                {transform: "rotate("+angle+" "+face.cx+" "+face.cy+")"},
                600,
                mina.elastic,
                function(){
                    if (angle === 360) {
                        seconds.attr({transform: "rotate("+0+" "+face.cx+" "+face.cy+")"});     
                    }
                }
            );
        }
        update();
        setInterval(update, 1000);
    }

    if ($('#face').length > 0) {
        watch();
    }

    if (popup.show)
    {
        $.fancybox(
                popup.content,
                {
                    overlayOpacity: .9,
                    overlayColor: settings.overlayColor,
                    padding: 20,
                    autoDimensions: !1,
                    width: popup.width,
                    height: popup.height,
                    transitionIn: "fade",
                    transitionOut: "fade",
                    onStart: function (a) {
                        $("#fancybox-outer").css({
                            background: popup.popupBackground
                        });
                        $("#fancybox-content").addClass("main-text no-margin");
                    }
                }
        );
    }

    $('#searchForm').on('click', 'button', function (e) {
        if (Modernizr.mq('(max-width: 479px)')) {
            e.stopPropagation();

            if (!$(e.delegateTarget).hasClass('show')) {
                e.preventDefault();
                $(e.delegateTarget).addClass('show');
                $(e.delegateTarget).find('input[type=text]').focus();
                $('body').one('click', function (e) {
                    $('#searchForm').removeClass('show');
                });
            }
        }
    });

    $('#searchForm').on('click', 'input', function (e) {
        e.stopPropagation();
    });
    
    var current_breakpoint = getCurrentBreakpoint();

    $(window).on('load', function() {
        $('svg').wrap('<div aria-hidden="true"></div>');
        updateUI();
    });

    $(window).on('resize', function() {

        var _cb = getCurrentBreakpoint();
        
        if (current_breakpoint !== _cb) {
            current_breakpoint = _cb;
            updateUI();
        }
        
    });

    function updateUI() {
        if (Modernizr.mq('(max-width: 991px)') && !isMobileView) {
            isMobileView = true;
            isMediumView = false;
            isLargeView = false;
            $('.header-address', '#banner').prependTo('.header-name', '#banner');
            $('.header-image-1').prependTo('.header-address', '#banner');
            $('.header-image-2').prependTo('.header-welcome .message');
            $('#searchbar').insertAfter('#fonts');
            $('#menu-top').insertBefore('#toolbar');
            $('.header__content').insertAfter('#banner');
            $('#content-holder').prependTo('#content-mobile');
            $('#sidebar-menu').prependTo('#sidebar-menu-mobile');
            $('.modules-top').prependTo('#modules-top-mobile');
            $('#modules-top2').prependTo('#modules-top2-mobile');
            $('#modules-bottom').prependTo('#modules-bottom-mobile');
        } 
        else if (Modernizr.mq('(min-width: 992px)') && isMobileView) {
            isMobileView = false;
            $('.header-address', '#banner').insertAfter('.header-name', '#banner');
            $('.header-image-1').insertAfter('.header-address', '#banner');
            $('.header-image-2').insertAfter('.header-address', '#banner');
            $('#searchbar').prependTo('#searchbar-desktop');
            $('#menu-top').insertAfter('#header');
            $('.header__content').insertBefore('#banner');
            $('#content-holder').prependTo('#content-desktop');
            $('#sidebar-menu').prependTo('#sidebar-menu-desktop');
            $('.modules-top').prependTo('#modules-top-desktop');
            $('#modules-top2').prependTo('#modules-top2-desktop');
            $('#modules-bottom').prependTo('#modules-bottom-desktop');
        }
        if (Modernizr.mq('(min-width: 1200px)') && !isLargeView) {
            isLargeView = true;
            isMediumView = false;
            equalizeHeightsWithPadding('.modules-top__holder');
            equalizeHeightsWithPadding('#modules-top2 .modules-content__module');
            equalizeHeightsWithPadding('#modules-bottom .modules-content__module');
        }
        else if (Modernizr.mq('(min-width: 992px)') && Modernizr.mq('(max-width: 1199px)') && !isMediumView) {
            isMediumView = true;
            isLargeView = false;
            equalizeHeightsWithPadding('.modules-top__holder');
            equalizeHeightsWithPadding('#modules-top2 .modules-content__module');
            equalizeHeightsWithPadding('#modules-bottom .modules-content__module');
        }
        if (Modernizr.mq('(min-width: 768px)') && Modernizr.mq('(max-width: 991px)')) {
            equalizeHeightsWithPadding('.sidebar-modules.row > div > div');
            equalizeHeightsWithPadding('.modules-top__holder');
            equalizeHeightsWithPadding('#modules-top2 .modules-content__module');
            equalizeHeightsWithPadding('#modules-bottom .modules-content__module');
        }
    }

    function moduleHeightAlign() {
        if (Modernizr.mq('(min-width: 768px)') && Modernizr.mq('(max-width: 991px)')) {

            var maxHeight = 0;
            var maxHeightModuleId = null;

            $('.sidebar-modules').find('.module').each(function () {
                var $this = $(this);
                if ($this.find('.module-body').height() > maxHeight) {
                    maxHeight = $this.find('.module-body').outerHeight();
                    maxHeightModuleId = $this.attr('id');
                }
            });

            $('.sidebar-modules').find('.module').each(function () {
                var $this = $(this);

                var moduleId = $this.attr('id');

                if (maxHeightModuleId !== moduleId) {

                    $this.find('.module-body').outerHeight(maxHeight);

                }
            });

            isModuleHeightAlign = true;

        } else if (isModuleHeightAlign) {

            $('.sidebar-modules').find('.module').each(function () {
                var $this = $(this);
                $this.outerHeight('auto');

            });

            isModuleHeightAlign = false;
        }
    }

    $('#navbar-top').on('click', '.dropdown-submenu > a', function (e) {

        var $this = $(this);
        var $submenu = $this.next('.dropdown-menu');


        if ($submenu.length > 0) {
            if (isMobile.any) {

                if ($this.parent().hasClass('open')) {
                    window.location.href = $this.attr('href');
                }

                $this.parent().addClass('open');

            } else {
                if ($this.parent().hasClass('open')) {
                    window.location.href = $this.attr('href');
                }
            }
        }

    });

    $('#sidebar-menu').on('click', 'a', function (e) {
        

        var $this = $(this);
        var $sublist = $this.next('.dropdown-menu');

        if ($sublist.is(':hidden')) {
            e.preventDefault();
            $sublist.show();
        }
        else if (!$sublist.is(':hidden')) {
            window.location.href = $this.attr('href');
        }

    });

    $('.menus a.selected', '#sidebar-menu').parents('.dropdown-menu').show();
    
});

function getCurrentBreakpoint() {
    
    var breakpoints = [0, 768, 992, 1200].reverse();
    
    for (var i=0; i < breakpoints.length; i++) {
        if (Modernizr.mq('(min-width: ' + breakpoints[i] + 'px)')) {
            return i;
        }
    }
}
