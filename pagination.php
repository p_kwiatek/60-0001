<?php
if ($pagination['end'] > 1)
{
    ?>
<div class="row">
    <div class="pagination-wrapper col-xs-12">
        <?php
        $active_page = $pagination['active'];

        if ( !isset($pagination['active']) )
        {
            $active_page = 1;
        }
        ?>
        <p><?php echo __('page')?>: <strong><?php echo $active_page?></strong>/<?php echo $pagination['end']?></p>
        <ul class="list-unstyled list-inline text-center">
        <?php
        if ($pagination['start'] != $pagination['prev'])
        {
            ?>
            <li>
                <a href="<?php echo $url . $pagination['start']?>" rel="nofollow" class="btn-first">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="pagination__background" viewBox="0 0 39 39">
                        <defs>
                            <filter filterUnits="userSpaceOnUse" id="pagination-first-filter" x="1.5px" y="1.5px" width="36px" height="36px">
                                <feOffset in="SourceAlpha" dx="0" dy="2" />
                                <feGaussianBlur result="blurOut" stdDeviation="0" />
                                <feFlood flood-color="rgb(0, 0, 0)" result="floodOut" />
                                <feComposite operator="atop" in="floodOut" in2="blurOut" />
                                <feComponentTransfer><feFuncA type="linear" slope="0.2"/></feComponentTransfer>
                                <feMerge>
                                    <feMergeNode/>
                                    <feMergeNode in="SourceGraphic"/>
                                </feMerge>
                            </filter>
                            <linearGradient id="pagination-first-gradient" x1="0%" x2="0%" y1="100%" y2="0%">
                                <stop offset="0%" stop-color="rgb(255,126,0)" stop-opacity="1" />
                                <stop offset="100%" stop-color="rgb(255,192,0)" stop-opacity="1" />
                            </linearGradient>
                        </defs>
                        <g filter="url(#pagination-first-filter)">
                            <path fill-rule="evenodd" d="M25.500,3.500 L13.500,3.500 C7.977,3.500 3.500,7.977 3.500,13.500 L3.500,25.500 C3.500,31.023 7.977,35.500 13.500,35.500 L25.500,35.500 C31.023,35.500 35.500,31.023 35.500,25.500 L35.500,13.500 C35.500,7.977 31.023,3.500 25.500,3.500 Z"/>
                        </g>
                        <path stroke-width="3px" fill="url(#pagination-first-gradient)" d="M25.500,3.500 L13.500,3.500 C7.977,3.500 3.500,7.977 3.500,13.500 L3.500,25.500 C3.500,31.023 7.977,35.500 13.500,35.500 L25.500,35.500 C31.023,35.500 35.500,31.023 35.500,25.500 L35.500,13.500 C35.500,7.977 31.023,3.500 25.500,3.500 Z"/>
                    </svg>
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="pagination__symbol" viewBox="0 0 10 12">
                        <defs>
                            <filter filterUnits="userSpaceOnUse" id="pagination-symbol-first-1" x="0px" y="0px" width="10px" height="12px">
                                <feOffset in="SourceAlpha" dx="0" dy="1" />
                                <feGaussianBlur result="blurOut" stdDeviation="0" />
                                <feFlood flood-color="rgb(255, 189, 0)" result="floodOut" />
                                <feComposite operator="atop" in="floodOut" in2="blurOut" />
                                <feComponentTransfer><feFuncA type="linear" slope="1"/></feComponentTransfer>
                                <feMerge>
                                    <feMergeNode/>
                                    <feMergeNode in="SourceGraphic"/>
                                </feMerge>
                            </filter>
                        </defs>
                        <g filter="url(#pagination-symbol-first-1)">
                            <path fill-rule="evenodd" d="M0.841,10.998 L6.339,5.500 L0.841,0.002 L4.504,0.002 L10.003,5.500 L4.504,10.998 L0.841,10.998 Z"/>
                        </g>
                    </svg>
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="pagination__symbol" viewBox="0 0 10 12">
                        <defs>
                            <filter filterUnits="userSpaceOnUse" id="pagination-symbol-first-2" x="0px" y="0px" width="10px" height="12px">
                                <feOffset in="SourceAlpha" dx="0" dy="1" />
                                <feGaussianBlur result="blurOut" stdDeviation="0" />
                                <feFlood flood-color="rgb(255, 189, 0)" result="floodOut" />
                                <feComposite operator="atop" in="floodOut" in2="blurOut" />
                                <feComponentTransfer><feFuncA type="linear" slope="1"/></feComponentTransfer>
                                <feMerge>
                                    <feMergeNode/>
                                    <feMergeNode in="SourceGraphic"/>
                                </feMerge>
                            </filter>
                        </defs>
                        <g filter="url(#pagination-symbol-first-2)">
                            <path fill-rule="evenodd" d="M0.841,10.998 L6.339,5.500 L0.841,0.002 L4.504,0.002 L10.003,5.500 L4.504,10.998 L0.841,10.998 Z"/>
                        </g>
                    </svg>
                    <span class="sr-only"><?php echo __('first page')?></span>
                </a>
            </li>
            <li>
                <a href="<?php echo $url . $pagination['prev']?>" rel="nofollow" class="btn-prev">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="pagination__background" viewBox="0 0 39 39">
                        <defs>
                            <filter filterUnits="userSpaceOnUse" id="pagination-prev-filter" x="1.5px" y="1.5px" width="36px" height="36px">
                                <feOffset in="SourceAlpha" dx="0" dy="2" />
                                <feGaussianBlur result="blurOut" stdDeviation="0" />
                                <feFlood flood-color="rgb(0, 0, 0)" result="floodOut" />
                                <feComposite operator="atop" in="floodOut" in2="blurOut" />
                                <feComponentTransfer><feFuncA type="linear" slope="0.2"/></feComponentTransfer>
                                <feMerge>
                                    <feMergeNode/>
                                    <feMergeNode in="SourceGraphic"/>
                                </feMerge>
                            </filter>
                            <linearGradient id="pagination-prev-gradient" x1="0%" x2="0%" y1="100%" y2="0%">
                                <stop offset="0%" stop-color="rgb(255,126,0)" stop-opacity="1" />
                                <stop offset="100%" stop-color="rgb(255,192,0)" stop-opacity="1" />
                            </linearGradient>
                        </defs>
                        <g filter="url(#pagination-prev-filter)">
                            <path fill-rule="evenodd" d="M25.500,3.500 L13.500,3.500 C7.977,3.500 3.500,7.977 3.500,13.500 L3.500,25.500 C3.500,31.023 7.977,35.500 13.500,35.500 L25.500,35.500 C31.023,35.500 35.500,31.023 35.500,25.500 L35.500,13.500 C35.500,7.977 31.023,3.500 25.500,3.500 Z"/>
                        </g>
                        <path stroke-width="3px" fill="url(#pagination-prev-gradient)" d="M25.500,3.500 L13.500,3.500 C7.977,3.500 3.500,7.977 3.500,13.500 L3.500,25.500 C3.500,31.023 7.977,35.500 13.500,35.500 L25.500,35.500 C31.023,35.500 35.500,31.023 35.500,25.500 L35.500,13.500 C35.500,7.977 31.023,3.500 25.500,3.500 Z"/>
                    </svg>
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="pagination__symbol" viewBox="0 0 10 12">
                        <defs>
                            <filter filterUnits="userSpaceOnUse" id="pagination-symbol-prev" x="0px" y="0px" width="10px" height="12px">
                                <feOffset in="SourceAlpha" dx="0" dy="1" />
                                <feGaussianBlur result="blurOut" stdDeviation="0" />
                                <feFlood flood-color="rgb(255, 189, 0)" result="floodOut" />
                                <feComposite operator="atop" in="floodOut" in2="blurOut" />
                                <feComponentTransfer><feFuncA type="linear" slope="1"/></feComponentTransfer>
                                <feMerge>
                                    <feMergeNode/>
                                    <feMergeNode in="SourceGraphic"/>
                                </feMerge>
                            </filter>
                        </defs>
                        <g filter="url(#pagination-symbol-prev)">
                            <path fill-rule="evenodd" d="M0.841,10.998 L6.339,5.500 L0.841,0.002 L4.504,0.002 L10.003,5.500 L4.504,10.998 L0.841,10.998 Z"/>
                        </g>
                    </svg>
                    <span class="sr-only"><?php echo __('prev page')?></span>
                </a>
            </li>
            <?php
        }		
        foreach ($pagination as $k => $v)
        {
            if (is_numeric($k))
            {
                ?>
            <li>
                <a href="<?php echo $url . $v?>" rel="nofollow" class="btn-page">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="pagination__background" viewBox="0 0 39 39">
                        <defs>
                            <filter filterUnits="userSpaceOnUse" id="<?php echo 'pagination-page-' . $v . '-filter'; ?>" x="1.5px" y="1.5px" width="36px" height="36px">
                                <feOffset in="SourceAlpha" dx="0" dy="2" />
                                <feGaussianBlur result="blurOut" stdDeviation="0" />
                                <feFlood flood-color="rgb(0, 0, 0)" result="floodOut" />
                                <feComposite operator="atop" in="floodOut" in2="blurOut" />
                                <feComponentTransfer><feFuncA type="linear" slope="0.2"/></feComponentTransfer>
                                <feMerge>
                                    <feMergeNode/>
                                    <feMergeNode in="SourceGraphic"/>
                                </feMerge>
                            </filter>
                            <linearGradient id="<?php echo 'pagination-page-' . $v . '-gradient'; ?>" x1="0%" x2="0%" y1="100%" y2="0%">
                                <stop offset="0%" stop-color="rgb(255,126,0)" stop-opacity="1" />
                                <stop offset="100%" stop-color="rgb(255,192,0)" stop-opacity="1" />
                            </linearGradient>
                        </defs>
                        <g filter="<?php echo 'url(#pagination-page-' . $v . '-filter)'; ?>">
                            <path fill-rule="evenodd" d="M25.500,3.500 L13.500,3.500 C7.977,3.500 3.500,7.977 3.500,13.500 L3.500,25.500 C3.500,31.023 7.977,35.500 13.500,35.500 L25.500,35.500 C31.023,35.500 35.500,31.023 35.500,25.500 L35.500,13.500 C35.500,7.977 31.023,3.500 25.500,3.500 Z"/>
                        </g>
                        <path stroke-width="3px" fill="<?php echo 'url(#pagination-page-' . $v . '-gradient)'; ?>" d="M25.500,3.500 L13.500,3.500 C7.977,3.500 3.500,7.977 3.500,13.500 L3.500,25.500 C3.500,31.023 7.977,35.500 13.500,35.500 L25.500,35.500 C31.023,35.500 35.500,31.023 35.500,25.500 L35.500,13.500 C35.500,7.977 31.023,3.500 25.500,3.500 Z"/>
                    </svg>
                    <span class="sr-only"><?php echo __('page')?></span>
                    <div><?php echo $v?></div>
                </a>
            </li>
                <?php
            } else if ($k == 'active')
            {
                ?>
            <li>
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="pagination__background" viewBox="0 0 39 39">
                    <defs>
                        <filter filterUnits="userSpaceOnUse" id="pagination-active-filter" x="1.5px" y="1.5px" width="36px" height="36px">
                            <feOffset in="SourceAlpha" dx="0" dy="2" />
                            <feGaussianBlur result="blurOut" stdDeviation="0" />
                            <feFlood flood-color="rgb(0, 0, 0)" result="floodOut" />
                            <feComposite operator="atop" in="floodOut" in2="blurOut" />
                            <feComponentTransfer><feFuncA type="linear" slope="0.2"/></feComponentTransfer>
                            <feMerge>
                                <feMergeNode/>
                                <feMergeNode in="SourceGraphic"/>
                            </feMerge>
                        </filter>
                        <linearGradient id="pagination-active-gradient" x1="0%" x2="0%" y1="100%" y2="0%">
                            <stop offset="0%" stop-color="rgb(56,75,0)" stop-opacity="1" />
                            <stop offset="100%" stop-color="rgb(98,130,0)" stop-opacity="1" />
                        </linearGradient>
                    </defs>
                    <g filter="url(#pagination-active-filter)">
                        <path fill-rule="evenodd"  fill="rgb(56, 75, 0)" d="M25.500,3.500 L13.500,3.500 C7.977,3.500 3.500,7.977 3.500,13.500 L3.500,25.500 C3.500,31.023 7.977,35.500 13.500,35.500 L25.500,35.500 C31.023,35.500 35.500,31.023 35.500,25.500 L35.500,13.500 C35.500,7.977 31.023,3.500 25.500,3.500 Z"/>
                    </g>
                    <path stroke-width="3px" stroke="rgb(56, 75, 0)" fill="url(#pagination-active-gradient)" d="M25.500,3.500 L13.500,3.500 C7.977,3.500 3.500,7.977 3.500,13.500 L3.500,25.500 C3.500,31.023 7.977,35.500 13.500,35.500 L25.500,35.500 C31.023,35.500 35.500,31.023 35.500,25.500 L35.500,13.500 C35.500,7.977 31.023,3.500 25.500,3.500 Z"/>
                </svg>
                <span class="sr-only"><?php echo __('page')?></span>
                <div class="page-active"><?php echo $v?></div>
            </li>
                <?php
            }			
        }		
        if ($pagination['active'] != $pagination['end'])
        {
            ?>
            <li>
                <a href="<?php echo $url . $pagination['next']?>" rel="nofollow" class="btn-next">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="pagination__background" viewBox="0 0 39 39">
                        <defs>
                            <filter filterUnits="userSpaceOnUse" id="pagination-next-filter" x="1.5px" y="1.5px" width="36px" height="36px">
                                <feOffset in="SourceAlpha" dx="0" dy="2" />
                                <feGaussianBlur result="blurOut" stdDeviation="0" />
                                <feFlood flood-color="rgb(0, 0, 0)" result="floodOut" />
                                <feComposite operator="atop" in="floodOut" in2="blurOut" />
                                <feComponentTransfer><feFuncA type="linear" slope="0.2"/></feComponentTransfer>
                                <feMerge>
                                    <feMergeNode/>
                                    <feMergeNode in="SourceGraphic"/>
                                </feMerge>
                            </filter>
                            <linearGradient id="pagination-next-gradient" x1="0%" x2="0%" y1="100%" y2="0%">
                                <stop offset="0%" stop-color="rgb(255,126,0)" stop-opacity="1" />
                                <stop offset="100%" stop-color="rgb(255,192,0)" stop-opacity="1" />
                            </linearGradient>
                        </defs>
                        <g filter="url(#pagination-next-filter)">
                            <path fill-rule="evenodd" d="M25.500,3.500 L13.500,3.500 C7.977,3.500 3.500,7.977 3.500,13.500 L3.500,25.500 C3.500,31.023 7.977,35.500 13.500,35.500 L25.500,35.500 C31.023,35.500 35.500,31.023 35.500,25.500 L35.500,13.500 C35.500,7.977 31.023,3.500 25.500,3.500 Z"/>
                        </g>
                        <path stroke-width="3px" fill="url(#pagination-next-gradient)" d="M25.500,3.500 L13.500,3.500 C7.977,3.500 3.500,7.977 3.500,13.500 L3.500,25.500 C3.500,31.023 7.977,35.500 13.500,35.500 L25.500,35.500 C31.023,35.500 35.500,31.023 35.500,25.500 L35.500,13.500 C35.500,7.977 31.023,3.500 25.500,3.500 Z"/>
                    </svg>
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="pagination__symbol" viewBox="0 0 10 12">
                        <defs>
                            <filter filterUnits="userSpaceOnUse" id="pagination-symbol-next" x="0px" y="0px" width="10px" height="12px">
                                <feOffset in="SourceAlpha" dx="0" dy="1" />
                                <feGaussianBlur result="blurOut" stdDeviation="0" />
                                <feFlood flood-color="rgb(255, 189, 0)" result="floodOut" />
                                <feComposite operator="atop" in="floodOut" in2="blurOut" />
                                <feComponentTransfer><feFuncA type="linear" slope="1"/></feComponentTransfer>
                                <feMerge>
                                    <feMergeNode/>
                                    <feMergeNode in="SourceGraphic"/>
                                </feMerge>
                            </filter>
                        </defs>
                        <g filter="url(#pagination-symbol-next)">
                            <path fill-rule="evenodd" d="M0.841,10.998 L6.339,5.500 L0.841,0.002 L4.504,0.002 L10.003,5.500 L4.504,10.998 L0.841,10.998 Z"/>
                        </g>
                    </svg>
                    <span class="sr-only"><?php echo __('next page')?></span>
                </a>
            </li>
            <li>
                <a href="<?php echo $url . $pagination['end']?>" rel="nofollow" class="btn-last">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="pagination__background" viewBox="0 0 39 39">
                        <defs>
                            <filter filterUnits="userSpaceOnUse" id="pagination-last-filter" x="1.5px" y="1.5px" width="36px" height="36px">
                                <feOffset in="SourceAlpha" dx="0" dy="2" />
                                <feGaussianBlur result="blurOut" stdDeviation="0" />
                                <feFlood flood-color="rgb(0, 0, 0)" result="floodOut" />
                                <feComposite operator="atop" in="floodOut" in2="blurOut" />
                                <feComponentTransfer><feFuncA type="linear" slope="0.2"/></feComponentTransfer>
                                <feMerge>
                                    <feMergeNode/>
                                    <feMergeNode in="SourceGraphic"/>
                                </feMerge>
                            </filter>
                            <linearGradient id="pagination-last-gradient" x1="0%" x2="0%" y1="100%" y2="0%">
                                <stop offset="0%" stop-color="rgb(255,126,0)" stop-opacity="1" />
                                <stop offset="100%" stop-color="rgb(255,192,0)" stop-opacity="1" />
                            </linearGradient>
                        </defs>
                        <g filter="url(#pagination-last-filter)">
                            <path fill-rule="evenodd" d="M25.500,3.500 L13.500,3.500 C7.977,3.500 3.500,7.977 3.500,13.500 L3.500,25.500 C3.500,31.023 7.977,35.500 13.500,35.500 L25.500,35.500 C31.023,35.500 35.500,31.023 35.500,25.500 L35.500,13.500 C35.500,7.977 31.023,3.500 25.500,3.500 Z"/>
                        </g>
                        <path stroke-width="3px" fill="url(#pagination-last-gradient)" d="M25.500,3.500 L13.500,3.500 C7.977,3.500 3.500,7.977 3.500,13.500 L3.500,25.500 C3.500,31.023 7.977,35.500 13.500,35.500 L25.500,35.500 C31.023,35.500 35.500,31.023 35.500,25.500 L35.500,13.500 C35.500,7.977 31.023,3.500 25.500,3.500 Z"/>
                    </svg>
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="pagination__symbol" viewBox="0 0 10 12">
                        <defs>
                            <filter filterUnits="userSpaceOnUse" id="pagination-symbol-last-1" x="0px" y="0px" width="10px" height="12px">
                                <feOffset in="SourceAlpha" dx="0" dy="1" />
                                <feGaussianBlur result="blurOut" stdDeviation="0" />
                                <feFlood flood-color="rgb(255, 189, 0)" result="floodOut" />
                                <feComposite operator="atop" in="floodOut" in2="blurOut" />
                                <feComponentTransfer><feFuncA type="linear" slope="1"/></feComponentTransfer>
                                <feMerge>
                                    <feMergeNode/>
                                    <feMergeNode in="SourceGraphic"/>
                                </feMerge>
                            </filter>
                        </defs>
                        <g filter="url(#pagination-symbol-last-1)">
                            <path fill-rule="evenodd" d="M0.841,10.998 L6.339,5.500 L0.841,0.002 L4.504,0.002 L10.003,5.500 L4.504,10.998 L0.841,10.998 Z"/>
                        </g>
                    </svg>
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="pagination__symbol" viewBox="0 0 10 12">
                        <defs>
                            <filter filterUnits="userSpaceOnUse" id="pagination-symbol-last-2" x="0px" y="0px" width="10px" height="12px">
                                <feOffset in="SourceAlpha" dx="0" dy="1" />
                                <feGaussianBlur result="blurOut" stdDeviation="0" />
                                <feFlood flood-color="rgb(255, 189, 0)" result="floodOut" />
                                <feComposite operator="atop" in="floodOut" in2="blurOut" />
                                <feComponentTransfer><feFuncA type="linear" slope="1"/></feComponentTransfer>
                                <feMerge>
                                    <feMergeNode/>
                                    <feMergeNode in="SourceGraphic"/>
                                </feMerge>
                            </filter>
                        </defs>
                        <g filter="url(#pagination-symbol-last-2)">
                            <path fill-rule="evenodd" d="M0.841,10.998 L6.339,5.500 L0.841,0.002 L4.504,0.002 L10.003,5.500 L4.504,10.998 L0.841,10.998 Z"/>
                        </g>
                    </svg>
                    <span class="sr-only"><?php echo __('last page')?></span>
                </a>
            </li>
            <?php
        }
        ?>	
        </ul>
        <div class="clearfix"></div>
    </div>
</div>
    <?php
}
?>