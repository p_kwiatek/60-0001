<?php
if ($showAll)
{
    ?>
<div class="gallery-wrapper row">
    <div class="col-xs-12">
        <h2 class="content-header"><span><?php echo $pageName?></span></h2>
        <ul class="list-unstyled gallery">
        <?php
        if (count($albums) > 0)
        {
            $n = 0;
            foreach ($albums as $value)
            {
                $n++;
                ?>
                <li>
                    <a href="<?php echo $value['link']?>" class="photo">
                        <svg class="photo-wrapper__hover" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="36px" height="38px">
                            <defs>
                                <filter filterUnits="userSpaceOnUse" id="<?php echo 'gallery-image-hover-filter-' . $n; ?>" x="0px" y="0px" width="36px" height="38px"  >
                                    <feOffset in="SourceAlpha" dx="0" dy="2" />
                                    <feGaussianBlur result="blurOut" stdDeviation="0" />
                                    <feFlood flood-color="rgb(0, 0, 0)" result="floodOut" />
                                    <feComposite operator="atop" in="floodOut" in2="blurOut" />
                                    <feComponentTransfer><feFuncA type="linear" slope="0.2"/></feComponentTransfer>
                                    <feMerge>
                                        <feMergeNode/>
                                        <feMergeNode in="SourceGraphic"/>
                                    </feMerge>
                                </filter>
                            </defs>
                            <g filter="<?php echo 'url(#gallery-image-hover-filter-' . $n .')'; ?>">
                                <path fill-rule="evenodd" d="M35.109,32.611 L32.611,35.108 C32.151,35.568 31.406,35.568 30.946,35.108 L25.952,30.114 C25.492,29.654 25.492,28.909 25.952,28.449 L26.086,28.314 L23.738,25.966 C23.679,25.906 23.646,25.833 23.604,25.764 C21.168,27.727 18.075,28.907 14.703,28.907 C6.859,28.907 0.500,22.548 0.500,14.703 C0.500,6.859 6.859,0.500 14.703,0.500 C22.548,0.500 28.907,6.859 28.907,14.703 C28.907,18.075 27.727,21.168 25.764,23.604 C25.833,23.646 25.906,23.679 25.966,23.738 L28.314,26.086 L28.449,25.952 C28.909,25.492 29.654,25.492 30.114,25.952 L35.109,30.946 C35.568,31.406 35.568,32.151 35.109,32.611 ZM14.703,5.971 C9.880,5.971 5.970,9.880 5.970,14.703 C5.970,19.526 9.880,23.436 14.703,23.436 C19.526,23.436 23.436,19.526 23.436,14.703 C23.436,9.880 19.526,5.971 14.703,5.971 Z"/>
                            </g>
                        </svg>
                        <img src="files/<?php echo $lang?>/mini/<?php echo $value['file']?>" alt="<?php echo $value['name']?>"/>
                        <p class="photo-name"><?php echo $value['name']?></p>
                    </a>
                </li>
                <?php
            }	
        } else
        {
            ?>
            <p><?php echo __('no photo album added')?></p>
            <?php
        }
        ?>
    </div>
</div>
    <?php
}
if ($showOne)
{
    ?>
<div class="gallery-wrapper row">
    <div class="col-xs-12">
        <h2 class="content-header"><span><?php echo $pageName?></span></h2>
        <?php 
        echo $message;
        ?>
	<?php 
	if ($showGallery)
	{
	    if (count($outRows) > 0)
	    {
                ?>
                <ul class="list-unstyled gallery">
                <?php
		$n = 0;
		foreach ($outRows as $value)
		{
		    $n++;
		    ?>
		    <li>
    			<a href="files/<?php echo $lang?>/<?php echo $value['file']?>" title="<?php echo __('enlarge image') . ': ' . $value['name']?>" data-fancybox-group="gallery" class="photo fancybox">
                    <svg class="photo-wrapper__hover" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="36px" height="38px">
                        <defs>
                            <filter filterUnits="userSpaceOnUse" id="<?php echo 'gallery-image-hover-filter-' . $n; ?>" x="0px" y="0px" width="36px" height="38px"  >
                                <feOffset in="SourceAlpha" dx="0" dy="2" />
                                <feGaussianBlur result="blurOut" stdDeviation="0" />
                                <feFlood flood-color="rgb(0, 0, 0)" result="floodOut" />
                                <feComposite operator="atop" in="floodOut" in2="blurOut" />
                                <feComponentTransfer><feFuncA type="linear" slope="0.2"/></feComponentTransfer>
                                <feMerge>
                                    <feMergeNode/>
                                    <feMergeNode in="SourceGraphic"/>
                                </feMerge>
                            </filter>
                        </defs>
                        <g filter="<?php echo 'url(#gallery-image-hover-filter-' . $n .')'; ?>">
                            <path fill-rule="evenodd" d="M35.109,32.611 L32.611,35.108 C32.151,35.568 31.406,35.568 30.946,35.108 L25.952,30.114 C25.492,29.654 25.492,28.909 25.952,28.449 L26.086,28.314 L23.738,25.966 C23.679,25.906 23.646,25.833 23.604,25.764 C21.168,27.727 18.075,28.907 14.703,28.907 C6.859,28.907 0.500,22.548 0.500,14.703 C0.500,6.859 6.859,0.500 14.703,0.500 C22.548,0.500 28.907,6.859 28.907,14.703 C28.907,18.075 27.727,21.168 25.764,23.604 C25.833,23.646 25.906,23.679 25.966,23.738 L28.314,26.086 L28.449,25.952 C28.909,25.492 29.654,25.492 30.114,25.952 L35.109,30.946 C35.568,31.406 35.568,32.151 35.109,32.611 ZM14.703,5.971 C9.880,5.971 5.970,9.880 5.970,14.703 C5.970,19.526 9.880,23.436 14.703,23.436 C19.526,23.436 23.436,19.526 23.436,14.703 C23.436,9.880 19.526,5.971 14.703,5.971 Z"/>
                        </g>
                    </svg>
                    <img src="files/<?php echo $lang?>/mini/<?php echo $value['file']?>" alt="<?php echo __('enlarge image') . ': ' . $value['name']?>" />
        			<?php
        			if (! check_html_text($value['name'], '') )
        			{
        			    ?>
        			    <p class="photo-name" aria-hidden="true"><?php echo $value['name']?></p>
        			    <?php
        			}
        		    ?>
                </a>
		    </li>
                <?php
		}
                ?>
                </ul>
                <?php
	    }
        }
    ?>
    </div>
</div>
<?php
if ($showLoginForm)
{
    ?>
    <div class="main-text">
    <?php
    include( CMS_TEMPL . DS . 'form_login.php');
    ?>
    </div>
    <?php
}
?>
    
<?php
}
?>
