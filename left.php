<div class="sidebar">
    <div id="modules-top-mobile"></div>
    <div id="modules-top2-mobile"></div>
    
    <div id="sidebar-menu-desktop">
        <div id="sidebar-menu" class="sidebar-menu clearfix">
            <svg class="border-decoration top" preserveAspectRatio="none" xmlns="http://www.w3.org/2000/svg" version="1.1" preserveAspectRatio="none" viewBox="0 0 263 32">
                <defs>
                    <linearGradient id="sidebar-menu-border-decoration-top-gradient" gradientUnits="userSpaceOnUse" x1="0%" y1="0%" x2="0%" y2="100%">
                        <stop offset="0%" stop-color="#ff7e00" stop-opacity="1" />
                        <stop offset="100%" stop-color="#f45c01" stop-opacity="1" />
                    </linearGradient>
                    <clipPath id="sidebar-menu-border-decoration-top-path">
                        <path d="M6,0c0,9.6-6,30-6,30s65.2-9.9,131.5-9.9C197.8,20.1,263,30,263,30s-6-20.4-6-30H6z"/>
                    </clipPath>
                </defs>
                <g transform="translate(0,1)">
                    <path opacity="0.2" d="M6,0c0,9.6-6,30-6,30s65.2-9.9,131.5-9.9C197.8,20.1,263,30,263,30s-6-20.4-6-30H6z"/>
                </g>
                <g>
                    <path fill="url(#sidebar-menu-border-decoration-top-gradient)" stroke-width="2px" d="M6,0c0,9.6-6,30-6,30s65.2-9.9,131.5-9.9C197.8,20.1,263,30,263,30s-6-20.4-6-30H6z" clip-path="url(#sidebar-menu-border-decoration-top-path)" />
                </g>
            </svg>
            <svg class="border-decoration bottom" preserveAspectRatio="none" xmlns="http://www.w3.org/2000/svg" version="1.1" preserveAspectRatio="none" viewBox="0 0 263 32">
                <defs>
                    <linearGradient id="sidebar-menu-border-decoration-bottom-gradient" gradientUnits="userSpaceOnUse" x1="0%" y1="0%" x2="0%" y2="100%">
                        <stop offset="0%" stop-color="#ff7e00" stop-opacity="1" />
                        <stop offset="100%" stop-color="#f45c01" stop-opacity="1" />
                    </linearGradient>
                    <clipPath id="sidebar-menu-border-decoration-bottom-path">
                        <path d="M6,0c0,9.6-6,30-6,30s65.2-9.9,131.5-9.9C197.8,20.1,263,30,263,30s-6-20.4-6-30H6z"/>
                    </clipPath>
                </defs>
                <g transform="translate(0,1)">
                    <path opacity="0.2" d="M6,0c0,9.6-6,30-6,30s65.2-9.9,131.5-9.9C197.8,20.1,263,30,263,30s-6-20.4-6-30H6z"/>
                </g>
                <g>
                    <path fill="url(#sidebar-menu-border-decoration-bottom-gradient)" stroke-width="2px" d="M6,0c0,9.6-6,30-6,30s65.2-9.9,131.5-9.9C197.8,20.1,263,30,263,30s-6-20.4-6-30H6z" clip-path="url(#sidebar-menu-border-decoration-bottom-path)" />
                </g>
            </svg>
            <span class="sr-only"><?php echo __('Menu') ?></span>
            <a id="add-menu" class="anchor" tabindex="-1"></a>
            <?php
                $caretLeft = '';
            ?>
            
            <?php get_menu_tree('mg', 0, 0, '', false, '', false, true, true, false, true, true, $caretLeft, true); ?>
            <?php
            /*
             * Dynamic menus
             */
            foreach ($menuType as $dynMenu) {
                if ($dynMenu['menutype'] != 'mg' && $dynMenu['menutype'] != 'tm') {
                    if ($dynMenu['active'] == 1) {

                        $menuClass = '';
                        if (strlen($dynMenu['name']) > 30) {
                            $menuClass = ' module-name-3';
                        } else if (strlen($dynMenu['name']) > 17 && strlen($dynMenu['name']) <= 30) {
                            $menuClass = ' module-name-2';
                        } else {
                            $menuClass = ' module-name-1';
                        }
                        
                        $caretLeft = '';
                        
                        ?>

                        <h2 class="module-name <?php echo $menuClass ?>"><?php echo $dynMenu['name'] ?></h2>

                        <?php get_menu_tree($dynMenu['menutype'], 0, 0, '', false, '', false, true, true, false, true, true, $caretLeft, true); ?>
                        <?php
                    }
                }
            }
            ?>
        </div>
    </div>
    <div class="sidebar-modules row">
    <?php
        foreach ($outRowLeftModules as $module) {
        
        $sidebar_module_number++;
        
        $modules_with_icons = array(
            'mod_calendar',
            'mod_stats',
            'mod_kzk',
            'mod_location',
            'mod_contact',
            'mod_forum',
            'mod_timetable',
            'mod_menu',
            'mod_newsletter',
            'mod_gallery',
            'mod_video',
            'mod_jokes',
            'mod_programs',
        );
        
        $modules_color2 = array(
            'mod_forum',
        );
        
        $href = '/^<a.*?href=(["\'])(.*?)\1.*$/';

        $link = '';

        $tmp = get_module($module['mod_name']);

        preg_match($href, $tmp, $m);

        if ($m[2] != '') {
            $link = $m[2];
        } else {
            $link = trans_url_name($module['name']);
        }
        
	?>
        <div class="col-sm-6 col-md-12">
            <div class="module <?php echo in_array($module['mod_name'], $modules_with_icons) ? 'module-with-icon module-common' : 'module-common'; ?><?php echo in_array($module['mod_name'], $modules_color2) ? ' color2' : ''; ?>" id="<?php echo $module['mod_name']; ?>">
                <svg class="border-decoration" preserveAspectRatio="none" xmlns="http://www.w3.org/2000/svg" version="1.1" preserveAspectRatio="none" viewBox="0 0 263 32">
                    <defs>
                        <?php if ($sidebar_module_number % 3 == 0): ?>
                            <linearGradient id="<?php echo 'sidebar-modules-border-decoration-gradient' . $module['mod_name']; ?>" gradientUnits="userSpaceOnUse" x1="0%" y1="0%" x2="0%" y2="100%">
                                <stop offset="0%" stop-color="#c04604" stop-opacity="1" />
                                <stop offset="100%" stop-color="#a33c04" stop-opacity="1" />
                            </linearGradient>
                        <?php endif; ?>
                        <?php if ($sidebar_module_number % 3 == 1): ?>
                            <linearGradient id="<?php echo 'sidebar-modules-border-decoration-gradient' . $module['mod_name']; ?>" gradientUnits="userSpaceOnUse" x1="0%" y1="0%" x2="0%" y2="100%">
                                <stop offset="0%" stop-color="#75a111" stop-opacity="1" />
                                <stop offset="100%" stop-color="#668c0f" stop-opacity="1" />
                            </linearGradient>
                        <?php endif; ?>
                        <?php if ($sidebar_module_number % 3 == 2): ?>
                            <linearGradient id="<?php echo 'sidebar-modules-border-decoration-gradient' . $module['mod_name']; ?>" gradientUnits="userSpaceOnUse" x1="0%" y1="0%" x2="0%" y2="100%">
                                <stop offset="0%" stop-color="#95a111" stop-opacity="1" />
                                <stop offset="100%" stop-color="#87920f" stop-opacity="1" />
                            </linearGradient>
                        <?php endif; ?>
                        <clipPath id="<?php echo 'sidebar-modules-border-decoration-path' . $module['mod_name']; ?>">
                            <path d="M6,0c0,9.6-6,30-6,30s65.2-9.9,131.5-9.9C197.8,20.1,263,30,263,30s-6-20.4-6-30H6z"/>
                        </clipPath>
                    </defs>
                    <g transform="translate(0,1)">
                        <path opacity="0.2" d="M6,0c0,9.6-6,30-6,30s65.2-9.9,131.5-9.9C197.8,20.1,263,30,263,30s-6-20.4-6-30H6z"/>
                    </g>
                    <g>
                        <path fill="<?php echo 'url(#sidebar-modules-border-decoration-gradient' . $module['mod_name'] . ')'; ?>" stroke-width="2px" d="M6,0c0,9.6-6,30-6,30s65.2-9.9,131.5-9.9C197.8,20.1,263,30,263,30s-6-20.4-6-30H6z" clip-path="<?php echo 'url(#sidebar-modules-border-decoration-path' . $module['mod_name'] . ')'; ?>" />
                    </g>
                </svg>
                <svg class="light--green" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 244 98">
                    <image  x="0px" y="0px" width="244px" height="98px"  xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPQAAABiCAYAAABnEP6gAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAAB3RJTUUH4QMVCQUERGC0KQAAGNZJREFUeNrtXW124zquhOT0rmb7bwVvOR3b82Mub8NIVaFIy4mTNs7xEcUvkRSLKIBUsv3f//8nXvKlskXElcRHStu86pblSp4x4nMbNxL/ki+W/asb8JKXvOQ4efvqBvwgydqq02gbKR8T8eg597ZfxW9mfCdsjF5ygLwA7UsF7JAM0I3kr2mrz67PVWmMPqs6Ud0qDrVjdRxZ21+gn5AX5X6sbME1Wxfn1H1vmUfb5Ue2/SWG/O0aGlFhpflmwehozZm2orajtD20o031d7af2XmHtHf3LKefYdb718tPB7RDezdwvZJ62KTOac5EXbE7nYnrTHK3v7UvrH+bUdaRrv5ugapjVcfvr5CfDuhVmXX4OGC9R5N0jqmVdigGsuq8Y2BkPoYhq+B/SZGfCGjlYZ6pw4lDz3Qo+NETcjPCQ67xh5LvTX0ri5Az3irdWYhyPU75v0Zj/0RAHykzE9oB6YzGX1mE1NZYjXd9A+iAi0PnVf6Xhn2QfGdAK7upo6LdnjAqO7OP3OXpnruiyTvb+CgQzdazqqlHeu6Leq8zJ9nUu/rWjrbvCOgVu04dilAOIFQH2opCbWB5toi4xOcAmrVHefdV/SueenfBvCdPNiNUf2fqrv39FgD/joB+pDCtll+qmjhDkG26kfTZE1pd+xlIGSA7QKNJ7Zz5XjEX1Jg9Smt+W22M5BkBPUMVHfvM1ehZ83be3pW96Rkn0L0OOKWlmcmhToChdnUe7c8ASW2jYltsIWNjp8Kf1b9peRZAs8l1bcrka5evi2N1bqFfcgf4lXunjSyt+2oqh7tJ6WhvJ98zHAJRDGIFrE+p2Z8F0Ew+0xvq2MgrGnzlvotjFFg5/BSgFGD3+J/NH6IcqvsK8qo8V1I36ufrAw8iXwVodytjxiZbPQzRhVV6p71ZfW4Z5+gpi59xYCmQV8dhfifd4qAAG/Hx/R4BTncHwRmDbgGZ9Uc8XD4T0Cu2sWsr5nvHFkTlZ/dlN5HW1efW0ZVX7Z31SM+Ak6UND34N5zxsUWCU+FEgUUdMnR0CJJuR56Hy7JT7CBke5c4JpLRtxDqgV6/o2e79ihwB6LoLsIH0IHFM63dllVmBvPw/Wj4b0K4DS1HnmUMk+d7Zp3UAWeM6R5pz7TT7LNhnD2zU+Hp/nYw78nrEfrBjtnVbclk2Iw4tNKvtt+VRgJ7dj9yaerpJzjzkLkhXQaeAuxtlx3VotZm2snG7B9ARHOAZZDV+BqDXO8vVPjLnm+p7Nye7E2r3HMt9qDwr5Xbs6052UCaDbPbagXgraXtTNkAZF9AzDEWJsmfztcY5gO7Ae4ke3DPgv8Qt3WfaXS0G314eCWjHa+14pXeQVsugk1cMOC5og5RRgNyb523Gc7pnob51485kFtAKbF1ajduafBF/nGqqbrRY3wvUrg5Gn5V54DrW7pKjAO3SarQyMlrdecRzOcdB5WhdF2g7KccAvcUcoGs+1gcVdmQF0LPgvZB0VXb8dlJePbsKAw/S3CiPI8zEZLZ2TT+MNXzlPnTt3Go9DBx11Z4FtALrLtL38ACtnocWjQjdh5XxdACNQDM+LrlED8oO0JfynHq/ifIh4up1UPL8YUwFEFsQguR5uE08K0cAurN30T1zZNW8nSaaodedxut+CtAor7qPpo4Ire2dMWByFXFMA0bcgpeB9ALquJSwKpMXGHbP6HnELUC3uN0Hzwtuxwgr8+u0pnIyIvu9e3/L8ggNjWgE6iCagAjkzO5mz3Bs4Y4ao/Q9OEBrPgboPKlWNHptezeeSJC5swJopFFZWKXXumqeIHU5GnuM29DKDp1Vc1fZx4hRRMnzcHk2L3fVojm+hh0aysDpamMFTBa3Bwe/Kl8XhAB5IjCgO1Ar+0x5qJFGHuFzSnMAq9IvpAwDtAvwbHrlvu+pDgbeWXkK+r0KaLfxnYMA5UFXlVYXgTH5K23qaPEsaMezKpB3UUYBfhftQpTdoeFKqgZWmo7RZgTKWu4SPXjHb4/b7aeRZy/1b8EXCUe29AxEhV0HFdPSQerM7+YqytW6bXk2DV2FDXRO72xhpu068CAAIvAqQKPwKbwFgNF41s+IdUB3TrBziruAeAXai4hz72u9GdAsrDR5Ny4uPf8KkW2bBXTX0Y76zVBqleZM7BnNi8IKmB2gTyBtAFkB2qHryr5G44tW/Oo4cjzVnebtAHxO92eQl9XRaXdEnzPAGW3P4zML3s6+ZnnQe+nSpxaYDtBMK3bi2CXbQjjbkZlSu3YxArdjD7PfieQ7mXHuwsH6UMfFBXS+Z1q5s4UzQC8ifsSd0v0eGLQK6OcyrzJg6zWDvfY59wExwEu5R07YTlBeh0qjPGjhoPKVlJvZHNmR4djCFegMzMz5NAvi/HuLP1qXgfcUGNAM4BtIV4vS6HcemzrOET2gkdeaOaoYcHMauj+H1tBnkpbt6Pwb/b0aV9TXiNuFYIxlzs9AjJ4T4QH+YaIArVYjtvqwDtd8m6g3QBlmJ3ZUW9mnpxL3VtIY8DqwnsDVLYMWhxrPWIfS0lWUZ7tS1AG0CA1EBcxz9MAeWvxM6t1B2pbiq5Zm/azARfvSER9BihxbM7TZiavxrC1UXMqt0jqbl1HoXJaBNYc7Da3odd2G6igvAqUC9Ft8BO+JxKkws8XRAqQAXbdp6vthE1/ZxFU7ZypcgXY2wjUutytT7Argca3zs9rMERjQ41rzzdq7jAUw4Dl0nQG3xtG6voOXOwvy9HbbPBWoEbdAqhSXAZrFsd9uxqN7B9AOsJEoz3am0Q6gEZARiM9G+l7qHP3P6UgTV1sazSGlsRUdR/XWRWPFqfYwQYCuNKN25irSWLkgeTsNzWjkrHbubFR2z4DZAXpo7DdS9i00oN9E25QzjwG6arJ6VfvKiEpfI+I9NKDZ7z1uQZp/Z3AdfbqA8Hh2nSPZth59HAtGvUYJZ3GpeE7LY15lI3WoMqw8lDdSoKvcSUOUeoQ7+3qFZjPb2fFU120lBUIE5jcR3ps8SpPXhQU58zpbGjl2mFebAZrZw0obv/8T/17usy38DspkcOe+ZXAPm772sfY9gzT7BhCoOzB1wmh5TnN9UiwOpd/ke3bKHfHRg6ucXlV7uWDO3mqHSv+KHtDs6oKeARrR8DwGeayYVFuyOsOqB1sBuoISAfr9n769/9PmUWaPj7R7hH/HLSirls7AfkthR1zbesQxwEU8Ed0eA5Eb2DkHwkiv+ZhNx5xnTEOjtFm6zSh1jVMaWAH4jcQpcKN6lQOt26Ou45nHr2rp7tBIZydnzzSLZ3T6vVxR20f+Ec9AW+cJ8xQP5XAJPj/ZPO3o9ezeMyvjCC3raGgH6Ftzj9IUYGs4StzKb5Z+K2pdAV1Bm+9/xR8t/Cs8LZ41dt3zrs4yxFIcDd3RbbQdxbRx1tTjitjEAPBG2tr5AUbbo4SrvVzzjPG5iGegZ3a7OGhcHc2N6mQLQreN9e0od16txz16ISt2c+cEczTxWxPHfh0tz8/dQPs+G9CDNme7edDprJGzZs4amgG5soz8TpFDKuL2bPnIcyp5gtxnYZoagYo5zlY07MMkA5ppYYeG1w7WsFPeWTkf4SRD3mtnW0rRc/T7FXoRqItFt7WVbegK6jpe9X1G3G7lKK/2uFZ7NzutBm3+HfgddoJOqu0l7Rq3oEX3aBsqHxxhPzbP9lRPdsBFYCDX9IhbzavKKOyxve6a54MNjWSVaquX6lBrdc3l7qXhCvBK43dA/hVzgEYAd/aqN3A/JINijFk9VKI+jlD7ydkGzmPINLHLGk4pLnu9M7hyOJ/L3sIDMAM02nJ1jnfW+A74tRyKZ3T/ac9yr4paNBzA7+GBmdnWq1o7A3eAG4FcOc2q9xuBudOQVYPV02GZZlfHFlpIEHVmz0VOOBQfKW70Nefdm3v0zq+kfQ5lZjT8CLp9BF2/saG7RiE1n9M6J0I14FeZAKtDOVZWtHW+V7Y38zp3VJ4tEuwZp6auTL2ZNqwaBIEhH9LI+8BsTpzKddSlxiM/p0vP7yDXXR1bs786bxDQO7rsUOh7xNl/hvneUqZZmV3Z3Gd01CznY2W3Jr+iXbOLA9o+OoE8Oc3R/o6Wdrav0LtAmjG3d4CGbRPVek7xR6NXrTru2UJ5r9nkvKdo8jJ67Ci7WQCr/B01V173f23oWacXqozdf1dRC8IRbODIH6O9uX3MqXMRfTlCG96jOd3FefY9rsqzeLJlO/am8OZWFHjwjvaQrywg9+TpyqKJyPIwrYLKqgVDPVulqz7MaDO3DOpL17aOnW2kPpa3m7P3pndtZXWszLeuni1CA/qnaNtHCjp5heJzerdPSu0j8hz1XHXEEeW9ijrZtas3JvM/iya8Rz4TO1MnxZSG7bQvcvd35dV+GyrD6kD3anJ1Za4H/ur2UHZAbeUe2bCjv3lvGHl3R96cxvqj/hRQ/VRypJ9LXP0TRdeU1xmX1XFm71EtjFcRf5QoB9qsDR3hMY4PXu4Zij3yKIOd5XXrrAOT0/O1blncA7bdiFNgyEDYQTh/rF8dUTkN2a8RfxxP+UXXsLI/x3iqPzWk9qbZnw1if8zgIsJs/I4A/AWUy/0PUCe7Z1LzbuW+K48EAbcD+r/hZ9yHdrRwV16l5Rdevbo5fAFhNqGRU0o5rJhdrBaL+tlfdVqdggNaTWIE6O6DjHdyVeGZH3o2A36OY0BmWn1VHmUW3E3VnZNiV5G+TeStA7E15ZBmrmlqVb1HSzNAI80agcGJNKRDw/MkfUvPHVtWQ9uPzxDrXvR49l6ei97DJTxAjyv6frle8+93if8N8jkaHn0oUoF870+NE1sQo4l3TYAgZfK9otz/xr9F/4BcqLNpI+a0K2ossru7ge4Anf9WVf37VQyYZxBf8yhvsOtl7mh8BnL94ipr/pN4PlocFd2ugL6G/u4ZgTp/E820uftnixi1V7/VxTyMeDQvXcBGUx7J1pT9Fz/PSLndgUB2dP0rFGxQmGY9x0eg5qtaqTvt+xYfwYq+UlKHSX6n8FbCzoKiFrxKX5FDbIZ+vwfX3PV3FmGXlncAv5B+VmVQj6B+K0FfW83YrJ33GjnM0HNyXPc8pJ3rlWno2m6kabNn+Vzy17BarRlYToG176DQznFPdUqs9q++h3y9iHa6H2nMauwZEDNKPgvkGdodJP7axDvzQbFOi1KT/DeUG2VkwuxaZ/uJLRQzAO/s6Dx4mVpXrb26sa+eWcGBAJw/eKjaOP9hQfR9NvsgAwE64uNChcYOebqRNkO2LaPUKMwA3DnWHKcaYhLK5r7X7u7mY4i4IzU/rOeZKTez2SNuJyj6SH2E2daNclaxLSoE2OG0GuDLoB17xOOPAow/EMDOanffZqsvrLo//oDGUJkILqCZncsAXf/WWAXvqON3qvM9NDtwqbbrJe9A99RUfOVviqlOMocYsnmr1kAavXseisu29PAIszqUp7L+6ldJjKIOEO/gflDq/AfzEICH8wv9tZL6uecjAK2ot/JIKwrebWUhbY8AjcKOJ5yZF0oTO2ko7IrC29LC8QYqQNy+hlFDlHeagTjMOEYZs9R92grwbmDH7xT4haIDJ2zi17bkraZsMytqjT6nrEAefwzgKEBH/PFqZzCcwVUBumrrC7iem/A1/gA7h2eoNdPIs0B1QezY0lmYWdkpVQr2r6TcquHViVVpdXYyZPtROb+YNqqf/mXw7iBfBizzSg8g/ibpiGaPPmStnD3ZTDOPPmZbOrddjb2joRmgx9Wl5BmYCMTM8TXC74EBfAbtVF5tpamjhF0a/jQyC2i1miDnVoj8rM5VyfUMjZwnNzqRxbzBNd8pMMARDR/aGP2tb0cLoz8xVEHtnDxznJt5oiIHWbahmXnBwuyQiAKuS6URU6iAZkB2nWIh4tgYxkJYvR8UL7W388/qZh6KKEe1jVkjFcVmcVmGdt7LPdq6yRo4AmvyDKpLCiP6jMA49rSVg6v7k0dIKyswR/SAHp7/PP6OExAtXEhbs/1rBt5a9j0+LhzOM6/keiF9UnEKtCytMsco+Wq4iqvYZL6voNxVq7v568DUo41V46KJfgXXER5aN8epP6mT7WCkScexzEqdEZDrs6o3u4J4b/qZr2xM69ipn0vDa75OyyLNr2xiZN93gM4+FAboSFdmP3fj+BTifD45ZIZGrw5At7mO7Oqtic82Zm7D0LyVkqMVN2voDLKLuK8OsXPwxWET9w7FrgDP/gX1Dhwwd0DJAFeefwR0VYY5tlZsYwXoTvN2jq9ssrADPEOQYnIOVNkLx4oNrSbJzMGRIPeq8cwrqLamqibO3xBXAFSAoiuj4uOUGdLWrLxydKmw0sz1cAmigIgGKhvyHB/Bwqg4A3QEB7gCtaORWThAmuorGg82Zh0W2FzMwua7C/QP8mwHS9yVSm1jZRt55GV0eyPhvO2EAJ3Hji0ADLhqoUDAdf6QXsTtZMlsxN0CVIDugDNAFtE7plY1rtLAyhZmgK792wL3vWON6r7Lf7jMAJoBiO07Ox1hmlq1AR1euZR6lAkwwFpBvIcGfMQt8EZ6dYx1wGWa1wF0pLgo8az/zoRUNFPZnC7d7a5ns84OvI4HezxnI/nzPAkQ74SjiXft8mkt/WwaWnVyM68RH7+aynXkQcvAyaAdLzvvb4+0Crj8rM7u7Wi10sxR0iLF1fC9gGZhBR617RXRA7sLR/SAvTR9yN79c8nj2Le5vhnH7qfJCqCZdnbyXEWZAPmc1U05HrKWzc89x0cAsTKXEkYAdECcbdwZOo0Azai2w1DQuCk6msMumCoQWT53UZj9hWh37T+7qjGqaWps3TTEPqflXg3tAJTZvffGVdAjpxDK33nrkfe8AjxT3mprZ8dYhAfabl8ZAZjRbXbP3gsKdxq7A6q7ANS6qtasdUb0QO7MB5RWxyDb1CidjZszT9W7uFvbf+U+NKPANV93XwehvgwEjhpGB0yUZsygzwCuIK5176LOrp2rgHbGbUVb57CjMR1Ar9ST64jgoGXt78bFYTuz8qh6DwE0A6jjGFuJm1k1nbZn6QBVAYQArEDK8oURRoDO1yD3amFk5osDAgdss5rVoc7sPkS5rm9OfjReYebtyh0mR2noWcqAFoFafjPuLzG/0qFnIy85C6O4a3z0nCNP9KXEdc9gefO1hqPE1zFGY+YAeyXO0azZ1FHl1DNZWoQPZBQ348dB5h+TjoIvy3fxciNRg5qBlelxHXDn6ua5NPfR1Om0AdUxFpM6BltgQOeJXzU1GkMXyChtFvA1j1snS0PhjeT7EfJIQM9Q8Kp5c5xazVwnxApzGPXPgD5C/xkglN/1WLO0K0hbMX9qvNLa1yZ+hbqj/DP1ovxh1sfGYWYOHWkKLsujNXQHWtRRRrMV6NFzFdjDjEd1dUCr7cyUXJVjFJrZyvl5LA/zEajJ5Wo0FXcJX7vX+AgfuCqMWAYblxkbdxaYam4dLt+ZcjvCHECIYjpaEsUpICrb9VGAZn3O6d2Y1fGbBbSbXik+K+tqTCduxe/ybeSrAO06D3J+pdUZTZ+Z0J3WqhQW9UGBtObpAIzGB1HsmXGs/enMmhn7s3OsoXBHjWfC6H42rlvwnMXQYQQPk6/W0B2wr2ZeNTE/0/FR26EAjfqiNPOV1LviUe0AXYGbnYsKIFdSPppwd380oFVex3ej6vpSR9tXA/poWR1MRc07J52rhVF+R9OyPWSWv7YZ/a01ZyxmFkblZLrH43yPg1P17cfKswCaaYetyYtkZY/vUauqovxsH515p5XWV2OgtM4RFLOLc+zwri4X0KuLDmv3bN+/XJ4F0Fkcz7hTHgFDTWb0gtCEP+pQAKKpqP8byc9E7SPPApqVU/1RfVTt78ozv4jT1s5ZyOqJyXJfLs8I6GeXS7lHbGJlcXAdYJ0wDb9iG+Z6nDJdXrcOdN+ZDfcwjR8j3wnQDCQqvzMBVvZqH92vnNaZH+6BHfeZte5OK6J6jwQ0qnt20XDZjeOpf2r5ToBG0lFy5YSpsmqvu3mdPsw8e/WZR4Fzpcy9fXLS3O0453DSt5PvDuhOHE8qKvOZ7athRpkv8fl23cp+bdWkXRtXzIqV7aS/Qn4SoDuHV2eTOg4VZpei/Kqdri34mdoD9Q3d30vB1ck9Z0zcZ9Y2zzoXv6Xs91fxkn9kBojXuAWG2sph+dy2uPmvxn1tx5XkDxH/COfV0xzs+Gr5SRo6C3rBHcjcU1DKSeU8x413+3VvnaqP6LrqaZ5t21FnCX60Rq7yUwGtxAW2otqsHmdfG5Vx9sQfraVRGXXfsQv3i7rZdjvbb38FeJG8KPe6HL3yd9Ra0XVGh90yqA1B8tT2uOPk5pmx0V9S5G/U0J0wmrl6ugjVreIdULt9mO2zcz9L+1e82E4fXqAH8gK0J659xoDAjp7mfIjSd0BwqOwMWJ3ns/1eVmbmlNlL7pQX5X7JS36QvDT048R1cs3kQeKcEe/21kdcxEfTgdWxQsNf8mB5aeiXvOQHyX8BUfPEC5q4sYoAAAAASUVORK5CYII=" />
                </svg>
                <svg class="light--red" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 244 98">
                    <image  x="0px" y="0px" width="244px" height="98px"  xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPQAAABiCAYAAABnEP6gAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAAB3RJTUUH4QMVCRAQaQ+GQAAAGNZJREFUeNrtXW124zquhOT0+mZbb7VvAx3b82Mub8NIVaFIy4mTNs7xEcUvkRSLKIBUsv3///0nXvKlskXElcRHStu86pblSp4x4nMbNxL/ki+W/asb8JKXvOQ4efvqBvwgydqq02gbKR8T8eg597ZfxW9mfCdsjF5ygLwA7UsF7JAM0I3kr2mrz67PVWmMPqs6Ud0qDrVjdRxZ21+gn5AX5X6sbME1Wxfn1H1vmUfb5Ue2/SWG/O0aGlFhpflmwehozZm2orajtD20o031d7af2XmHtHf3LKefYdb718tPB7RDezdwvZJ62KTOac5EXbE7nYnrTHK3v7UvrH+bUdaRrv5ugapjVcfvr5CfDuhVmXX4OGC9R5N0jqmVdigGsuq8Y2BkPoYhq+B/SZGfCGjlYZ6pw4lDz3Qo+NETcjPCQ67xh5LvTX0ri5Az3irdWYhyPU75v0Zj/0RAHykzE9oB6YzGX1mE1NZYjXd9A+iAi0PnVf6Xhn2QfGdAK7upo6LdnjAqO7OP3OXpnruiyTvb+CgQzdazqqlHeu6Leq8zJ9nUu/rWjrbvCOgVu04dilAOIFQH2opCbWB5toi4xOcAmrVHefdV/SueenfBvCdPNiNUf2fqrv39FgD/joB+pDCtll+qmjhDkG26kfTZE1pd+xlIGSA7QKNJ7Zz5XjEX1Jg9Smt+W22M5BkBPUMVHfvM1ehZ83be3pW96Rkn0L0OOKWlmcmhToChdnUe7c8ASW2jYltsIWNjp8Kf1b9peRZAs8l1bcrka5evi2N1bqFfcgf4lXunjSyt+2oqh7tJ6WhvJ98zHAJRDGIFrE+p2Z8F0Ew+0xvq2MgrGnzlvotjFFg5/BSgFGD3+J/NH6IcqvsK8qo8V1I36ufrAw8iXwVodytjxiZbPQzRhVV6p71ZfW4Z5+gpi59xYCmQV8dhfifd4qAAG/Hx/R4BTncHwRmDbgGZ9Uc8XD4T0Cu2sWsr5nvHFkTlZ/dlN5HW1efW0ZVX7Z31SM+Ak6UND34N5zxsUWCU+FEgUUdMnR0CJJuR56Hy7JT7CBke5c4JpLRtxDqgV6/o2e79ihwB6LoLsIH0IHFM63dllVmBvPw/Wj4b0K4DS1HnmUMk+d7Zp3UAWeM6R5pz7TT7LNhnD2zU+Hp/nYw78nrEfrBjtnVbclk2Iw4tNKvtt+VRgJ7dj9yaerpJzjzkLkhXQaeAuxtlx3VotZm2snG7B9ARHOAZZDV+BqDXO8vVPjLnm+p7Nye7E2r3HMt9qDwr5Xbs6052UCaDbPbagXgraXtTNkAZF9AzDEWJsmfztcY5gO7Ae4ke3DPgv8Qt3WfaXS0G314eCWjHa+14pXeQVsugk1cMOC5og5RRgNyb523Gc7pnob51485kFtAKbF1ajduafBF/nGqqbrRY3wvUrg5Gn5V54DrW7pKjAO3SarQyMlrdecRzOcdB5WhdF2g7KccAvcUcoGs+1gcVdmQF0LPgvZB0VXb8dlJePbsKAw/S3CiPI8zEZLZ2TT+MNXzlPnTt3Go9DBx11Z4FtALrLtL38ACtnocWjQjdh5XxdACNQDM+LrlED8oO0JfynHq/ifIh4up1UPL8YUwFEFsQguR5uE08K0cAurN30T1zZNW8nSaaodedxut+CtAor7qPpo4Ire2dMWByFXFMA0bcgpeB9ALquJSwKpMXGHbP6HnELUC3uN0Hzwtuxwgr8+u0pnIyIvu9e3/L8ggNjWgE6iCagAjkzO5mz3Bs4Y4ao/Q9OEBrPgboPKlWNHptezeeSJC5swJopFFZWKXXumqeIHU5GnuM29DKDp1Vc1fZx4hRRMnzcHk2L3fVojm+hh0aysDpamMFTBa3Bwe/Kl8XhAB5IjCgO1Ar+0x5qJFGHuFzSnMAq9IvpAwDtAvwbHrlvu+pDgbeWXkK+r0KaLfxnYMA5UFXlVYXgTH5K23qaPEsaMezKpB3UUYBfhftQpTdoeFKqgZWmo7RZgTKWu4SPXjHb4/b7aeRZy/1b8EXCUe29AxEhV0HFdPSQerM7+YqytW6bXk2DV2FDXRO72xhpu068CAAIvAqQKPwKbwFgNF41s+IdUB3TrBziruAeAXai4hz72u9GdAsrDR5Ny4uPf8KkW2bBXTX0Y76zVBqleZM7BnNi8IKmB2gTyBtAFkB2qHryr5G44tW/Oo4cjzVnebtAHxO92eQl9XRaXdEnzPAGW3P4zML3s6+ZnnQe+nSpxaYDtBMK3bi2CXbQjjbkZlSu3YxArdjD7PfieQ7mXHuwsH6UMfFBXS+Z1q5s4UzQC8ifsSd0v0eGLQK6OcyrzJg6zWDvfY59wExwEu5R07YTlBeh0qjPGjhoPKVlJvZHNmR4djCFegMzMz5NAvi/HuLP1qXgfcUGNAM4BtIV4vS6HcemzrOET2gkdeaOaoYcHMauj+H1tBnkpbt6Pwb/b0aV9TXiNuFYIxlzs9AjJ4T4QH+YaIArVYjtvqwDtd8m6g3QBlmJ3ZUW9mnpxL3VtIY8DqwnsDVLYMWhxrPWIfS0lWUZ7tS1AG0CA1EBcxz9MAeWvxM6t1B2pbiq5Zm/azARfvSER9BihxbM7TZiavxrC1UXMqt0jqbl1HoXJaBNYc7Da3odd2G6igvAqUC9Ft8BO+JxKkws8XRAqQAXbdp6vthE1/ZxFU7ZypcgXY2wjUutytT7Argca3zs9rMERjQ41rzzdq7jAUw4Dl0nQG3xtG6voOXOwvy9HbbPBWoEbdAqhSXAZrFsd9uxqN7B9AOsJEoz3am0Q6gEZARiM9G+l7qHP3P6UgTV1sazSGlsRUdR/XWRWPFqfYwQYCuNKN25irSWLkgeTsNzWjkrHbubFR2z4DZAXpo7DdS9i00oN9E25QzjwG6arJ6VfvKiEpfI+I9NKDZ7z1uQZp/Z3AdfbqA8Hh2nSPZth59HAtGvUYJZ3GpeE7LY15lI3WoMqw8lDdSoKvcSUOUeoQ7+3qFZjPb2fFU120lBUIE5jcR3ps8SpPXhQU58zpbGjl2mFebAZrZw0obv/8T/17usy38DspkcOe+ZXAPm772sfY9gzT7BhCoOzB1wmh5TnN9UiwOpd/ke3bKHfHRg6ucXlV7uWDO3mqHSv+KHtDs6oKeARrR8DwGeayYVFuyOsOqB1sBuoISAfr9n769/9PmUWaPj7R7hH/HLSirls7AfkthR1zbesQxwEU8Ed0eA5Eb2DkHwkiv+ZhNx5xnTEOjtFm6zSh1jVMaWAH4jcQpcKN6lQOt26Ou45nHr2rp7tBIZydnzzSLZ3T6vVxR20f+Ec9AW+cJ8xQP5XAJPj/ZPO3o9ezeMyvjCC3raGgH6Ftzj9IUYGs4StzKb5Z+K2pdAV1Bm+9/xR8t/Cs8LZ41dt3zrs4yxFIcDd3RbbQdxbRx1tTjitjEAPBG2tr5AUbbo4SrvVzzjPG5iGegZ3a7OGhcHc2N6mQLQreN9e0od16txz16ISt2c+cEczTxWxPHfh0tz8/dQPs+G9CDNme7edDprJGzZs4amgG5soz8TpFDKuL2bPnIcyp5gtxnYZoagYo5zlY07MMkA5ppYYeG1w7WsFPeWTkf4SRD3mtnW0rRc/T7FXoRqItFt7WVbegK6jpe9X1G3G7lKK/2uFZ7NzutBm3+HfgddoJOqu0l7Rq3oEX3aBsqHxxhPzbP9lRPdsBFYCDX9IhbzavKKOyxve6a54MNjWSVaquX6lBrdc3l7qXhCvBK43dA/hVzgEYAd/aqN3A/JINijFk9VKI+jlD7ydkGzmPINLHLGk4pLnu9M7hyOJ/L3sIDMAM02nJ1jnfW+A74tRyKZ3T/ac9yr4paNBzA7+GBmdnWq1o7A3eAG4FcOc2q9xuBudOQVYPV02GZZlfHFlpIEHVmz0VOOBQfKW70Nefdm3v0zq+kfQ5lZjT8CLp9BF2/saG7RiE1n9M6J0I14FeZAKtDOVZWtHW+V7Y38zp3VJ4tEuwZp6auTL2ZNqwaBIEhH9LI+8BsTpzKddSlxiM/p0vP7yDXXR1bs786bxDQO7rsUOh7xNl/hvneUqZZmV3Z3Gd01CznY2W3Jr+iXbOLA9o+OoE8Oc3R/o6Wdrav0LtAmjG3d4CGbRPVek7xR6NXrTru2UJ5r9nkvKdo8jJ67Ci7WQCr/B01V173f23oWacXqozdf1dRC8IRbODIH6O9uX3MqXMRfTlCG96jOd3FefY9rsqzeLJlO/am8OZWFHjwjvaQrywg9+TpyqKJyPIwrYLKqgVDPVulqz7MaDO3DOpL17aOnW2kPpa3m7P3pndtZXWszLeuni1CA/qnaNtHCjp5heJzerdPSu0j8hz1XHXEEeW9ijrZtas3JvM/iya8Rz4TO1MnxZSG7bQvcvd35dV+GyrD6kD3anJ1Za4H/ur2UHZAbeUe2bCjv3lvGHl3R96cxvqj/hRQ/VRypJ9LXP0TRdeU1xmX1XFm71EtjFcRf5QoB9qsDR3hMY4PXu4Zij3yKIOd5XXrrAOT0/O1blncA7bdiFNgyEDYQTh/rF8dUTkN2a8RfxxP+UXXsLI/x3iqPzWk9qbZnw1if8zgIsJs/I4A/AWUy/0PUCe7Z1LzbuW+K48EAbcD+r/hZ9yHdrRwV16l5Rdevbo5fAFhNqGRU0o5rJhdrBaL+tlfdVqdggNaTWIE6O6DjHdyVeGZH3o2A36OY0BmWn1VHmUW3E3VnZNiV5G+TeStA7E15ZBmrmlqVb1HSzNAI80agcGJNKRDw/MkfUvPHVtWQ9uPzxDrXvR49l6ei97DJTxAjyv6frle8+93if8N8jkaHn0oUoF870+NE1sQo4l3TYAgZfK9otz/xr9F/4BcqLNpI+a0K2ossru7ge4Anf9WVf37VQyYZxBf8yhvsOtl7mh8BnL94ipr/pN4PlocFd2ugL6G/u4ZgTp/E820uftnixi1V7/VxTyMeDQvXcBGUx7J1pT9Fz/PSLndgUB2dP0rFGxQmGY9x0eg5qtaqTvt+xYfwYq+UlKHSX6n8FbCzoKiFrxKX5FDbIZ+vwfX3PV3FmGXlncAv5B+VmVQj6B+K0FfW83YrJ33GjnM0HNyXPc8pJ3rlWno2m6kabNn+Vzy17BarRlYToG176DQznFPdUqs9q++h3y9iHa6H2nMauwZEDNKPgvkGdodJP7axDvzQbFOi1KT/DeUG2VkwuxaZ/uJLRQzAO/s6Dx4mVpXrb26sa+eWcGBAJw/eKjaOP9hQfR9NvsgAwE64uNChcYOebqRNkO2LaPUKMwA3DnWHKcaYhLK5r7X7u7mY4i4IzU/rOeZKTez2SNuJyj6SH2E2daNclaxLSoE2OG0GuDLoB17xOOPAow/EMDOanffZqsvrLo//oDGUJkILqCZncsAXf/WWAXvqON3qvM9NDtwqbbrJe9A99RUfOVviqlOMocYsnmr1kAavXseisu29PAIszqUp7L+6ldJjKIOEO/gflDq/AfzEICH8wv9tZL6uecjAK2ot/JIKwrebWUhbY8AjcKOJ5yZF0oTO2ko7IrC29LC8QYqQNy+hlFDlHeagTjMOEYZs9R92grwbmDH7xT4haIDJ2zi17bkraZsMytqjT6nrEAefwzgKEBH/PFqZzCcwVUBumrrC7iem/A1/gA7h2eoNdPIs0B1QezY0lmYWdkpVQr2r6TcquHViVVpdXYyZPtROb+YNqqf/mXw7iBfBizzSg8g/ibpiGaPPmStnD3ZTDOPPmZbOrddjb2joRmgx9Wl5BmYCMTM8TXC74EBfAbtVF5tpamjhF0a/jQyC2i1miDnVoj8rM5VyfUMjZwnNzqRxbzBNd8pMMARDR/aGP2tb0cLoz8xVEHtnDxznJt5oiIHWbahmXnBwuyQiAKuS6URU6iAZkB2nWIh4tgYxkJYvR8UL7W388/qZh6KKEe1jVkjFcVmcVmGdt7LPdq6yRo4AmvyDKpLCiP6jMA49rSVg6v7k0dIKyswR/SAHp7/PP6OExAtXEhbs/1rBt5a9j0+LhzOM6/keiF9UnEKtCytMsco+Wq4iqvYZL6voNxVq7v568DUo41V46KJfgXXER5aN8epP6mT7WCkScexzEqdEZDrs6o3u4J4b/qZr2xM69ipn0vDa75OyyLNr2xiZN93gM4+FAboSFdmP3fj+BTifD45ZIZGrw5At7mO7Oqtic82Zm7D0LyVkqMVN2voDLKLuK8OsXPwxWET9w7FrgDP/gX1Dhwwd0DJAFeefwR0VYY5tlZsYwXoTvN2jq9ssrADPEOQYnIOVNkLx4oNrSbJzMGRIPeq8cwrqLamqibO3xBXAFSAoiuj4uOUGdLWrLxydKmw0sz1cAmigIgGKhvyHB/Bwqg4A3QEB7gCtaORWThAmuorGg82Zh0W2FzMwua7C/QP8mwHS9yVSm1jZRt55GV0eyPhvO2EAJ3Hji0ADLhqoUDAdf6QXsTtZMlsxN0CVIDugDNAFtE7plY1rtLAyhZmgK792wL3vWON6r7Lf7jMAJoBiO07Ox1hmlq1AR1euZR6lAkwwFpBvIcGfMQt8EZ6dYx1wGWa1wF0pLgo8az/zoRUNFPZnC7d7a5ns84OvI4HezxnI/nzPAkQ74SjiXft8mkt/WwaWnVyM68RH7+aynXkQcvAyaAdLzvvb4+0Crj8rM7u7Wi10sxR0iLF1fC9gGZhBR617RXRA7sLR/SAvTR9yN79c8nj2Le5vhnH7qfJCqCZdnbyXEWZAPmc1U05HrKWzc89x0cAsTKXEkYAdECcbdwZOo0Azai2w1DQuCk6msMumCoQWT53UZj9hWh37T+7qjGqaWps3TTEPqflXg3tAJTZvffGVdAjpxDK33nrkfe8AjxT3mprZ8dYhAfabl8ZAZjRbXbP3gsKdxq7A6q7ANS6qtasdUb0QO7MB5RWxyDb1CidjZszT9W7uFvbf+U+NKPANV93XwehvgwEjhpGB0yUZsygzwCuIK5176LOrp2rgHbGbUVb57CjMR1Ar9ST64jgoGXt78bFYTuz8qh6DwE0A6jjGFuJm1k1nbZn6QBVAYQArEDK8oURRoDO1yD3amFk5osDAgdss5rVoc7sPkS5rm9OfjReYebtyh0mR2noWcqAFoFafjPuLzG/0qFnIy85C6O4a3z0nCNP9KXEdc9gefO1hqPE1zFGY+YAeyXO0azZ1FHl1DNZWoQPZBQ348dB5h+TjoIvy3fxciNRg5qBlelxHXDn6ua5NPfR1Om0AdUxFpM6BltgQOeJXzU1GkMXyChtFvA1j1snS0PhjeT7EfJIQM9Q8Kp5c5xazVwnxApzGPXPgD5C/xkglN/1WLO0K0hbMX9qvNLa1yZ+hbqj/DP1ovxh1sfGYWYOHWkKLsujNXQHWtRRRrMV6NFzFdjDjEd1dUCr7cyUXJVjFJrZyvl5LA/zEajJ5Wo0FXcJX7vX+AgfuCqMWAYblxkbdxaYam4dLt+ZcjvCHECIYjpaEsUpICrb9VGAZn3O6d2Y1fGbBbSbXik+K+tqTCduxe/ybeSrAO06D3J+pdUZTZ+Z0J3WqhQW9UGBtObpAIzGB1HsmXGs/enMmhn7s3OsoXBHjWfC6H42rlvwnMXQYQQPk6/W0B2wr2ZeNTE/0/FR26EAjfqiNPOV1LviUe0AXYGbnYsKIFdSPppwd380oFVex3ej6vpSR9tXA/poWR1MRc07J52rhVF+R9OyPWSWv7YZ/a01ZyxmFkblZLrH43yPg1P17cfKswCaaYetyYtkZY/vUauqovxsH515p5XWV2OgtM4RFLOLc+zwri4X0KuLDmv3bN+/XJ4F0Fkcz7hTHgFDTWb0gtCEP+pQAKKpqP8byc9E7SPPApqVU/1RfVTt78ozv4jT1s5ZyOqJyXJfLs8I6GeXS7lHbGJlcXAdYJ0wDb9iG+Z6nDJdXrcOdN+ZDfcwjR8j3wnQDCQqvzMBVvZqH92vnNaZH+6BHfeZte5OK6J6jwQ0qnt20XDZjeOpf2r5ToBG0lFy5YSpsmqvu3mdPsw8e/WZR4Fzpcy9fXLS3O0453DSt5PvDuhOHE8qKvOZ7athRpkv8fl23cp+bdWkXRtXzIqV7aS/Qn4SoDuHV2eTOg4VZpei/Kqdri34mdoD9Q3d30vB1ck9Z0zcZ9Y2zzoXv6Xs91fxkn9kBojXuAWG2sph+dy2uPmvxn1tx5XkDxH/COfV0xzs+Gr5SRo6C3rBHcjcU1DKSeU8x413+3VvnaqP6LrqaZ5t21FnCX60Rq7yUwGtxAW2otqsHmdfG5Vx9sQfraVRGXXfsQv3i7rZdjvbb38FeJG8KPe6HL3yd9Ra0XVGh90yqA1B8tT2uOPk5pmx0V9S5G/U0J0wmrl6ugjVreIdULt9mO2zcz9L+1e82E4fXqAH8gK0J659xoDAjp7mfIjSd0BwqOwMWJ3ns/1eVmbmlNlL7pQX5X7JS36QvDT048R1cs3kQeKcEe/21kdcxEfTgdWxQsNf8mB5aeiXvOQHyX8BZr/D/Pqf9a4AAAAASUVORK5CYII=" />
                </svg>
                <div class="module-body clearfix">
                    <?php if (in_array($module['mod_name'], $modules_with_icons)): ?>
                        <h2 class="module-name"><?php echo $module['name'] ?></h2>
                        <div class="module-content"><div class="module__icon"></div><?php echo get_module($module['mod_name']) ?></div>
                    <?php else: ?>
                        <h2 class="module-name"><?php echo $module['name'] ?></h2>
                        <div class="module-content"><?php echo get_module($module['mod_name']) ?></div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    <?php
        }
    ?>
    </div>
    
    <div id="modules-bottom-mobile"></div>
    
    <?php
    if (count($leftAdv) > 0)
    {
	?>
	<div id="advertsLeftWrapper">
	    <div id="advertsLeftContent">
	<?php
	$n = 0;
	foreach ($leftAdv as $adv)
	{
	    ?>
	    <div class="advertLeft">
	    <?php
	    $extUrl = '';
	    if ($adv['ext_url'] != '')
	    {
		$newWindow = '';
		if ($adv['new_window'] == 1)
		{
		    $newWindow = ' target="_blank" ';
		}
	    			
		$swfLink = '';
		$swfSize = '';
		if ($adv['attrib'] != '' && $adv['type'] == 'flash')
		{
		    //$swfLink = ' style="width:'.$swfSize[0].'px; height:'.$swfSize[1].'px; z-index:1; display:block; position:absolute; left:0; top:0" ';
		}
					
		$extUrl = '<a href="'.$adv['ext_url'].'"'.$newWindow.$swfLink.'>';
		$extUrlEnd = '</a>';
	    }
				
	    switch ($adv['type'])
	    {
		case 'flash':
		    $swfSize = explode(',', $adv['attrib']);
		    //echo $extUrl;
		    ?>
		    <div id="advLeft_<?php echo $n?>"><?php echo $adv['attrib']?></div>
		    <script type="text/javascript">
                        swfobject.embedSWF("<?php echo $adv['content'] . '?noc=' . time()?>", "advLeft_<?php echo $n?>", "<?php echo $swfSize[0]?>", "<?php echo $swfSize[1]?>", "9.0.0", "expressInstall.swf", {}, {wmode:"transparent"}, {});
		    </script>
		    <?php			
		    //echo $extUrlEnd;
		    break;
					
		case 'html':
		    echo $adv['content'];
		    break;
					
		case 'image':
		default:
		    $width = '';
		    if ($adv['content'] != '')
		    {                    
                        $size = getimagesize($adv['content']);
                        if ($size[0] > $templateConfig['maxWidthLeftAdv'])
                        {
                            $width = ' width="'.$templateConfig['maxWidthLeftAdv'].'" ';
                        } 
                        echo $extUrl;
                        ?>
                        <img src="<?php echo $adv['content']?>" alt="<?php echo $adv['name']?>" <?php echo $width?>/>
                        <?php
                        echo $extUrlEnd;
		    }
		    break;
	    }
	    ?>
	    </div>
	    <?php
	    $n++;
	}
	?>
	    </div>
	    
	</div>
        
	<?php
    }
    ?>

</div>
