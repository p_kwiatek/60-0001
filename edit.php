<?php
if ($changeAvatar){
?>
<script type="text/javascript">
    $(document).ready(function() {
	$('.userAvatar img').attr('src', 'files/<?php echo $lang; ?>/avatars/mini/<?php echo $avatar; ?>');		
    });
</script>
<?php
}
if ($deleteAvatar){
?>
<script type="text/javascript">
    $(document).ready(function() {
	$('.userAvatar img').attr('src', 'files/avatar-<?php echo $sex; ?>.png');		
    });
</script>
<?php
}

?>

<a tabindex="-1" id="edytuj"></a>
<?php
echo '<h2 class="content-header"><span>' . $pageName . '</span></h2>';

if ($showEditForm){
?>
    <form id="editForm" name="editForm" method="post" action="<?php echo $url; ?>,aktualizuj#edytuj" enctype="multipart/form-data">
	<fieldset>
	    <?php
	    echo $message;
	    ?>
	    <legend><?php echo __('edit action')?></legend>
	    
            <div class="formL"><span class="formLabel"><?php echo __('login')?>:</span></div>
	    <div class="formR login"><?php echo $outRow['login']; ?></div>
	    
	    <div class="formL"><label for="password" class="formLabel"><span class="asterisk">*</span><?php echo __('password'); ?>:</label></div>
	    <div class="formR"><input type="password" id="password" name="password" class="inText" size="35" maxlength="50" value="<?php echo $password; ?>" /><span id="passwordError" class="msgMarg"></span>
		<span class="comment"><?php echo __('password change info'); ?></span>
	    </div>
	    
	    <div class="formL"><span class="asterisk">*</span><label for="password_confirm" class="formLabel"><?php echo __('repeat password'); ?>:</label></div>
	    <div class="formR"><input type="password" id="password_confirm" name="password_confirm" class="inText" size="35" maxlength="50" value="<?php echo $password_confirm; ?>" /><span id="passwordConfirmError" class="msgMarg"></span></div>
	    
	    <div class="formL"><span class="asterisk">*</span><label for="email" class="formLabel"><?php echo __('email'); ?>:</label></div>
	    <div class="formR"><input type="text" id="email" name="email" class="inText" size="35" maxlength="100" value="<?php echo $email; ?>" /><span id="emailError" class="msgMarg"></span></div>
	    
	    <div class="formL"><label for="first_name" class="formLabel"><?php echo __('firstname'); ?>:</label></div>
	    <div class="formR"><input type="text" id="first_name" name="first_name" class="inText" size="35" maxlength="50" value="<?php echo $first_name; ?>" /></div>
	    
	    <div class="formL"><label for="last_name" class="formLabel"><?php echo __('lastname'); ?>:</label></div>
	    <div class="formR"><input type="text" id="last_name" name="last_name" class="inText" size="35" maxlength="50" value="<?php echo $last_name; ?>" /></div>
	    
            <div class="formL"><span class="formLabel"><?php echo __('gender'); ?>:</span></div>
	    <div class="formR radio">
	    	<div>
				<input type="radio" id="sex_m" name="sex" value="m" <?php if ($sex == 'm'){ echo 'checked="checked"';} ?> />
				<label for="sex_m" id="l_sex_m"><?php echo __('man'); ?></label>
			</div>
			<div>
				<input type="radio" id="sex_f" name="sex" value="f" <?php if ($sex == 'f'){ echo 'checked="checked"';} ?> />
				<label for="sex_f" id="l_sex_f"><?php echo __('woman'); ?></label>
			</div>
	    </div>
	    
	    <?php
	    if ($avatar != ''){
	    ?>
	    <input type="hidden" name="avatar" value="<?php echo $avatar; ?>" />
            <div class="formL"><span class="formLabel"><?php echo __('avatar'); ?>:</span></div>
	    <div class="formR p10"><span class="avatarFrameEdit"></span><img src="files/<?php echo $lang; ?>/avatars/mini/<?php echo $avatar; ?>" alt="Avatar użytkownika" class="avatarFrameImage" /><a href="<?php echo $url; ?>,usun" title="Usuń avatar" class="delete"><?php echo __('delete avatar'); ?></a></div>
	    
	    <?php
	    } else {
	    ?>
	    
	    <div class="formL"><label for="avatar" class="formLabel"><?php echo __('avatar'); ?>:</label></div>
	    <div class="formR">
		
		<input type="text" id="avatar" name="avatar" size="35" readonly="readonly" class="inText" />
		<div class="file_input_div">
                    <input id="btnFilePos" class="butForm" type="button" value="<?php echo __('choose'); ?>..." name="fake" />
			<input id="avatar_f" class="avatar_f" type="file" onchange="javascript: document.getElementById('avatar').value = this.value" name="avatar_f" size="36" tabindex="-1" />
		</div>
		<div class="comment"><?php echo __('file info'); ?></div>
	    </div>

	    
	    <?php
	    }
	    ?>
	    
	    <div class="formL"><span class="asterisk">*</span><?php echo __('required fields'); ?></div>
	    <div class="formR"><input type="submit" name="ok" value="<?php echo __('update action'); ?>" class="butForm"/></div>	    
	    
	</fieldset>
    </form>

<script type="text/javascript">
    $(document).ready(function() {
	var form = $('#editForm');
	form.submit(function(){
	    if (validatePass() && validatePasswords() && validateEmail()){
		//return true;
	    } else {
		return false;
	    }
	});
	
	$('#password').blur(validatePass);
	function validatePass(){
	    var value = $('#password').val();
	    if (value.length > 0){
		if (value.length < 9 || value.length > 50){
		    $('#password').addClass('inError');
		    $('#passwordError').addClass('msgError').text('<?php echo __('error min length password'); ?>');
		    return false;
		} else {
		    validatePasswords();
		    $('#password').removeClass('inError');
		    $('#passwordError').removeClass('msgError').text('');
		    return true;
		}
	    } else {
		    $('#password').removeClass('inError');
		    $('#passwordError').removeClass('msgError').text('');
		    return true;		
	    }
	}

	$('#password_confirm').blur(validatePasswords);
	function validatePasswords(){
	    if ($('#password').val() != $('#password_confirm').val()){
		$('#password_confirm').addClass('inError');
		$('#passwordConfirmError').addClass('msgError').text('<?php echo __('error passwords dont match'); ?>');
		return false;
	    } else {
		$('#password_confirm').removeClass('inError');
		$('#passwordConfirmError').removeClass('msgError').text('');
		return true;
	    }
	}
	
	$('#email').blur(validateEmail);
	function validateEmail(){
	    var exp = /^[a-zA-Z0-9]+[a-zA-Z0-9_.-]+[a-zA-Z0-9_-]+@[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{2,4}$/;
	    var email = $("#email").val();
	    if (!exp.test(email)){
		$('#email').addClass('inError');
		$('#emailError').addClass('msgError').text('<?php echo __('error incorrect email'); ?>');
		return false;
	    } else {
		$('#email').removeClass('inError');
		$('#emailError').removeClass('msgError').text('');
		return true;
	    }
	}	
    });
</script>	
<?php
} else {
    echo $message;
}
?>

