<a id="top"></a>    
<div id="popup"></div>

<div class="top-wrapper">
    <div class="header-wrapper clearfix">       
        <div id="header" class="header">
            <div class="header__background"></div> 
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="header__content">
                            <?php
                            $pageInfo['name'] = str_replace('w ', 'w&nbsp;', $pageInfo['name']);
                            $pageInfo['name'] = str_replace('im ', 'im&nbsp;', $pageInfo['name']);
                            if (strlen($pageInfo['name']) > 60) {
                            ?>
                                <p class="name"><?php echo $pageInfo['name']; ?></p>
                            <?php
                            }
                            if (strlen($pageInfo['name']) > 60) {
                            ?>
                                <div class="header__address header__address--small">
                                    <?php
                                    echo $headerAddress;
                                    if ($pageInfo['email'] != '') {
                                        echo '<a href="mailto:' . $pageInfo['email'] . '">' . $pageInfo['email'] . '</a>';
                                    }
                                    ?>
                                </div>
                            <?php
                            }
                            else {
                            ?>
                                <div class="header__address">
                                    <?php
                                    echo $headerAddress;
                                    if ($pageInfo['email'] != '') {
                                        echo '<a href="mailto:' . $pageInfo['email'] . '">' . $pageInfo['email'] . '</a>';
                                    }
                                    ?>
                                </div>
                            <?php
                            }
                            ?> 
                        </div>
                        <div id="banner">
                            <svg class="banner__back" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 706.121 491.656">
                                <path stroke-linejoin="round" stroke-width="3px" d="M1146.14-110.291c193.6,0,362.35,134.414,376.92,300.222s-130.55,300.223-324.15,300.223S836.563,355.74,821.991,189.931,952.547-110.291,1146.14-110.291Z" transform="translate(-819.469)"/>
                            </svg>
                            <svg class="banner__outer" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 735.224 502.906">
                            <defs>
                                <filter id="banner-outer-filter" x="835.906" y="-33.031" width="735.224" height="535.937" filterUnits="userSpaceOnUse">
                                    <feGaussianBlur result="blur" stdDeviation="0.333" in="SourceAlpha"/>
                                    <feFlood result="flood" flood-color="#fff" flood-opacity="0.03"/>
                                    <feComposite result="composite-2" operator="out" in2="blur"/>
                                    <feComposite result="composite" operator="in" in2="SourceAlpha"/>
                                    <feBlend result="blend" in2="SourceGraphic"/>
                                    <feGaussianBlur result="blur-2" in="SourceAlpha"/>
                                    <feFlood result="flood-2" flood-color="#fff" flood-opacity="0.09"/>
                                    <feComposite result="composite-3" operator="out" in2="blur-2"/>
                                    <feOffset result="offset" dy="1"/>
                                    <feComposite result="composite-4" operator="in" in2="SourceAlpha"/>
                                    <feBlend result="blend-2" in2="blend"/>
                                </filter>
                              </defs>
                              <path filter="url(#banner-outer-filter)" d="M1175.92-33.017c202.43,0,378.89,119.97,394.13,267.961S1433.53,502.9,1231.1,502.9s-378.9-119.97-394.136-267.96S973.481-33.017,1175.92-33.017Z" transform="translate(-835.906)"/>
                            </svg>
                            <?php if ($outBannerTopRows == 0): ?>
                                <?php
                                    $img = $templateDir . '/' . 'images' . '/' . 'banner-default.jpg';
                                ?>
                                <div class="carousel-content">
                                    <div class="banner__photo">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 700 537">
                                            <pattern id="header-banner" patternUnits="userSpaceOnUse" width="100%" height="100%">
                                                <image height="100%" width="100%" preserveAspectRatio="xMidYMid slice" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $img; ?>"></image>
                                            </pattern>
                                            <path fill="url(#header-banner)" d="M323.819,0.242 C516.042,0.242 683.598,120.211 698.066,268.202 C712.534,416.193 568.436,536.163 376.214,536.163 C183.991,536.163 16.435,416.193 1.967,268.202 C-12.502,120.211 131.597,0.242 323.819,0.242 Z"></path>
                                        </svg>
                                    </div>
                                </div>
                            <?php elseif ($outBannerTopRows == 1): ?>
                                <?php
                                    $value = $outBannerTop[0];
                                    $img = 'files' . '/' . $lang . '/' . 'mini' . '/' . urldecode($value['photo']);
                                ?>
                                <div class="carousel-content">
                                    <div class="banner__photo">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 700 535">
                                            <pattern id="header-banner" patternUnits="userSpaceOnUse" width="100%" height="100%">
                                                <image width="100%" height="100%" preserveAspectRatio="xMidYMid slice" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $img; ?>"></image>
                                            </pattern>
                                            <path fill="url(#header-banner)" d="M323.819,0.242 C516.042,0.242 683.598,120.211 698.066,268.202 C712.534,416.193 568.436,536.163 376.214,536.163 C183.991,536.163 16.435,416.193 1.967,268.202 C-12.502,120.211 131.597,0.242 323.819,0.242 Z"></path>
                                        </svg>
                                    </div>
                                </div>
                            <?php else: ?>
                                <div class="carousel-content owl-carousel">
                                    <?php foreach ($outBannerTop as $value): ?>
                                        <?php
                                            $i++;
                                            $img = 'files' . '/' . $lang . '/' . 'mini' . '/' . urldecode($value['photo']);
                                        ?>
                                        <div class="banner__photo">
                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 700 535">
                                                <pattern id="<?php echo 'header-banner-' . $i; ?>" patternUnits="userSpaceOnUse" width="100%" height="100%">
                                                    <image height="100%" width="100%" preserveAspectRatio="xMidYMid slice" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $img; ?>"></image>
                                                </pattern>
                                                <path fill="<?php echo 'url(#header-banner-' . $i . ')'; ?>" d="M323.819,0.242 C516.042,0.242 683.598,120.211 698.066,268.202 C712.534,416.193 568.436,536.163 376.214,536.163 C183.991,536.163 16.435,416.193 1.967,268.202 C-12.502,120.211 131.597,0.242 323.819,0.242 Z"></path>
                                        </svg>
                                        </div>
                                    <?php endforeach ?>
                                </div>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="menu-top" class="menu-top">
            <a id="main-menu" tabindex="-1" style="display: inline;"></a>
            <nav class="navbar">
                <p class="sr-only"><?php echo __('main menu'); ?></p>
                <div class="navbar-header">
                    <button class="navbar-toggle collapsed" aria-controls="navbar-top" aria-expanded="false" data-target="#navbar-top" data-toggle="collapse" type="button">
                        <i class="icon" aria-hidden="true"></i>
                        <i class="icon" aria-hidden="true"></i>
                        <i class="icon" aria-hidden="true"></i>
                        <span class="sr-only"><?php echo __('expand')?></span>
                        <span class="title sr-only"><?php echo __('main menu')?></span>
                    </button>
                </div>
                <div id="navbar-top" class="collapse navbar-collapse menu clearfix">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <svg class="navbar__outer" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="none" viewBox="0 0 1170 98">
                                    <defs>
                                        <filter id="filter-navbar-outer" x="416" y="452" width="1170" height="98" filterUnits="userSpaceOnUse">
                                            <feImage preserveAspectRatio="none" x="416" y="452" width="1170" height="98" result="image" xlink:href="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB3aWR0aD0iMTE3MCIgaGVpZ2h0PSI5OCIgdmlld0JveD0iMCAwIDExNzAgOTgiPgogIDxkZWZzPgogICAgPHN0eWxlPgogICAgICAuY2xzLTEgewogICAgICAgIGZpbGw6IHVybCgjbGluZWFyLWdyYWRpZW50KTsKICAgICAgfQogICAgPC9zdHlsZT4KICAgIDxsaW5lYXJHcmFkaWVudCBpZD0ibGluZWFyLWdyYWRpZW50IiB4MT0iNTg1IiB4Mj0iNTg1IiB5Mj0iOTgiIGdyYWRpZW50VW5pdHM9InVzZXJTcGFjZU9uVXNlIj4KICAgICAgPHN0b3Agb2Zmc2V0PSIwIiBzdG9wLWNvbG9yPSIjY2FmODQ2Ii8+CiAgICAgIDxzdG9wIG9mZnNldD0iMSIgc3RvcC1jb2xvcj0iIzZlOTgwZSIvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICA8L2RlZnM+CiAgPHJlY3QgY2xhc3M9ImNscy0xIiB3aWR0aD0iMTE3MCIgaGVpZ2h0PSI5OCIvPgo8L3N2Zz4K"/>
                                            <feComposite result="composite" operator="in" in2="SourceGraphic"/>
                                            <feBlend result="blend" in2="SourceGraphic"/>
                                        </filter>
                                    </defs>
                                    <path class="navbar__outer--path" filter="url(#filter-navbar-outer)" d="M1560.59,501c0,15.679,25.4,49,25.4,49s-260-11.232-584.99-11.232S416.01,550,416.01,550s25.4-33.322,25.4-49-25.4-49-25.4-49,260,11.233,584.99,11.233S1585.99,452,1585.99,452,1560.59,485.321,1560.59,501Z" transform="translate(-416 -452)"/>
                                </svg>
                                <svg class="navbar__inner" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="none" viewBox=" 0 0 1277 67">
                                    <defs>
                                        <filter filterUnits="userSpaceOnUse" id="filter-navbar-inner" x="1.5px" y="1.5px" width="1274px" height="64px">
                                            <feOffset in="SourceAlpha" dx="0" dy="2" />
                                            <feGaussianBlur result="blurOut" stdDeviation="0" />
                                            <feFlood flood-color="rgb(0, 0, 0)" result="floodOut" />
                                            <feComposite operator="atop" in="floodOut" in2="blurOut" />
                                            <feComponentTransfer><feFuncA type="linear" slope="0.3"/></feComponentTransfer>
                                            <feMerge>
                                            <feMergeNode/>
                                            <feMergeNode in="SourceGraphic"/>
                                            </feMerge>
                                        </filter>
                                        <linearGradient id="gradient-nav-inner" x1="0%" x2="0%" y1="100%" y2="0%">
                                            <stop offset="0%" stop-color="rgb(255,126,0)" stop-opacity="1" />
                                            <stop offset="100%" stop-color="rgb(255,192,0)" stop-opacity="1" />
                                        </linearGradient>
                                    </defs>
                                    <g filter="url(#filter-navbar-inner)">
                                        <path class="navbar__inner--path1" fill-rule="evenodd" d="M1248.092,33.500 C1248.092,43.099 1273.490,63.500 1273.490,63.500 C1273.490,63.500 963.494,56.624 638.504,56.624 C313.507,56.624 3.510,63.500 3.510,63.500 C3.510,63.500 28.910,43.099 28.910,33.500 C28.910,23.900 3.510,3.499 3.510,3.499 C3.510,3.499 313.507,10.377 638.504,10.377 C963.494,10.377 1273.490,3.499 1273.490,3.499 C1273.490,3.499 1248.092,23.900 1248.092,33.500 Z"/>
                                    </g>
                                    <path class="navbar__inner--path2" stroke-width="3px" stroke="rgb(255, 126, 0)" fill="url(#gradient-nav-inner)" d="M1248.092,33.500 C1248.092,43.099 1273.490,63.500 1273.490,63.500 C1273.490,63.500 963.494,56.624 638.504,56.624 C313.507,56.624 3.510,63.500 3.510,63.500 C3.510,63.500 28.910,43.099 28.910,33.500 C28.910,23.900 3.510,3.499 3.510,3.499 C3.510,3.499 313.507,10.377 638.504,10.377 C963.494,10.377 1273.490,3.499 1273.490,3.499 C1273.490,3.499 1248.092,23.900 1248.092,33.500 Z"/>
                                </svg>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-push-3 col-md-9">
                                <?php get_menu_tree('tm', 0, 0, '', false, '', false, true, true, false, true, true, '', true); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
    </div>

    <div class="page-content page-content-mobile">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div id="content-mobile"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div id="sidebar-menu-mobile" class="sidebar"></div>
                </div>
            </div>
        </div>
    </div>

</div>
