<h2 class="content-header"><span><?php echo $pageName; ?></span></h2>
<?php
if ($showMessage)
{
?>
<div class="txt_com"><p><?php echo __('no jokes info'); ?></p></div>
<?php
}
?>

<?php
if ($showList)
{
?>
<ul id="jokes">
    <?php
    foreach ($outRows as $row)
    {
    ?>
    <li>
	<div class="jokeText"><?php echo $row['text']?></div>
	<div class="jokeNick"><?php echo __('added by'); ?>: <strong><?php echo $row['nick']?></strong>, <span class="jokeDate"><?php echo __('day added'); ?>: <?php echo substr($row['date_add'], 0, 10)?></span></div>
    </li>
    <?php
    //strip_tags(str_replace(array("\n", "\r"), "", $_POST['text'])),
    }
    ?>
</ul>
<?php
$url = $PHP_SELF.'?c=' . $_GET['c'] . '&amp;mod=mod_jokes&amp;s=';
include (CMS_TEMPL . DS . 'pagination.php');	
}
?>

<?php
if ($showAddForm)
{
?>
<a id="dodaj" tabindex="-1" class="anchor"></a>
<h3 class="subHead"><?php echo __('add joke'); ?></h3>
<form  name="formAddJoke" id="formAddJoke" class="" method="post" action="humor,dodaj-zart#dodaj">
    <?php
    echo $message;
    ?>    
    <fieldset>
	<legend><?php echo __('add joke'); ?></legend>
	
	<div class="formL">
	    <label for="nick" class="formLabel"><span class="asterisk">*</span><?php echo __('nick'); ?>:</label>
	</div>
	<div class="formR">
	    <input type="text" id="nick" name="nick" class="inText inLong" size="35" maxlength="50" value="<?php echo $nick?>"/><span id="nickMsg" class="msgMarg"></span>
	</div>
	
	<div class="formL">
	    <label for="text" class="formLabel"><span class="asterisk">*</span><?php echo __('content'); ?>:</label>
	</div>
	<div class="formR">
	    <textarea id="text" name="text" rows="8" cols="40" class="inTextArea inLong"><?php echo $text?></textarea><span id="textMsg" class="msgMarg"></span>
	</div>
	
	<div class="formL"></div>
	<div class="formR"><p><?php echo __('math info'); ?></p></div>
	
	<div class="formL">
	    <label for="topicCaptcha" class="formLabel"><span class="asterisk">*</span><?php echo __('math result'); ?>:</label>
	</div>
	<div class="formR"><span class="captchaTxt"><strong><?php echo $captchaTxt; ?></strong> <?php echo __('is'); ?></span><input type="text" id="topicCaptcha" name="captcha" size="2" maxlength="2" class="inTextSmall" /><span id="topicCaptchaMsg" class="msgMarg"></span></div>

	<div class="formL">
	    <span class="asterisk">*</span><?php echo __('required fields'); ?>
	</div>
	<div class="formR">
	    <input type="submit" name="ok" value="<?php echo __('add'); ?>" class="butForm" />
	</div>
    
    </fieldset>
</form>
<?php
}
?>
<script type="text/javascript">
    $(document).ready(function() {
		var form = $('#formAddJoke');
		form.submit(function() {
		    if (validateNick() && validateText() && validateCaptcha()){
			//return true;
		    } else {
		       return false;
		    }
		});
		
		$('#nick').blur(validateNick);
		function validateNick(){
		    var value = $('#nick').val();
		    if (value == ''){
			$('#nick').addClass('inError');
			$('#nickMsg').addClass('msgError').text('<?php echo __('error nick'); ?>');
			return false;
		    } else {
			$('#nick').removeClass('inError');
			$('#nickMsg').removeClass('msgError').text('');
			return true;
		    }
		}
		
		$('#text').blur(validateText);
		function validateText(){
		    var value = $('#text').val();
		    if (value == ''){
			$('#text').addClass('inError');
			$('#textMsg').addClass('msgError').text('<?php echo __('error joke'); ?>');
			return false;
		    } else {
			$('#text').removeClass('inError');
			$('#textMsg').removeClass('msgError').text('');
			return true;
		    }
		}

		$('#topicCaptcha').blur(validateCaptcha);
		function validateCaptcha(){
		    var value = $('#topicCaptcha').val();
		    if (value == ''){
			$('#topicCaptcha').addClass('inError');
			$('#topicCaptchaMsg').addClass('msgError').text('<?php echo __('error captcha'); ?>');
			return false;
		    } else {
			$('#topicCaptcha').removeClass('inError');
			$('#topicCaptchaMsg').removeClass('msgError').text('');
			return true;
		    }
		}
    });	
</script>