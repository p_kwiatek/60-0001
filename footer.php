<footer class="page-footer">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <svg class="footer__inner" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="none" viewBox=" 0 0 1277 67">
                    <defs>
                        <filter filterUnits="userSpaceOnUse" id="filter-footer-inner" x="1.5px" y="1.5px" width="1274px" height="64px">
                            <feOffset in="SourceAlpha" dx="0" dy="2" />
                            <feGaussianBlur result="blurOut" stdDeviation="0" />
                            <feFlood flood-color="rgb(0, 0, 0)" result="floodOut" />
                            <feComposite operator="atop" in="floodOut" in2="blurOut" />
                            <feComponentTransfer><feFuncA type="linear" slope="0.3"/></feComponentTransfer>
                            <feMerge>
                            <feMergeNode/>
                            <feMergeNode in="SourceGraphic"/>
                            </feMerge>
                        </filter>
                        <linearGradient id="gradient-footer-inner" x1="0%" x2="0%" y1="100%" y2="0%">
                            <stop offset="0%" stop-color="rgb(255,126,0)" stop-opacity="1" />
                            <stop offset="100%" stop-color="rgb(255,192,0)" stop-opacity="1" />
                        </linearGradient>
                    </defs>
                    <g filter="url(#filter-footer-inner)">
                        <path class="footer__inner--path1" fill-rule="evenodd" d="M1248.092,33.500 C1248.092,43.099 1273.490,63.500 1273.490,63.500 C1273.490,63.500 963.494,56.624 638.504,56.624 C313.507,56.624 3.510,63.500 3.510,63.500 C3.510,63.500 28.910,43.099 28.910,33.500 C28.910,23.900 3.510,3.499 3.510,3.499 C3.510,3.499 313.507,10.377 638.504,10.377 C963.494,10.377 1273.490,3.499 1273.490,3.499 C1273.490,3.499 1248.092,23.900 1248.092,33.500 Z"/>
                    </g>
                    <path class="footer__inner--path2" stroke-width="3px" stroke="rgb(255, 126, 0)" fill="url(#gradient-footer-inner)" d="M1248.092,33.500 C1248.092,43.099 1273.490,63.500 1273.490,63.500 C1273.490,63.500 963.494,56.624 638.504,56.624 C313.507,56.624 3.510,63.500 3.510,63.500 C3.510,63.500 28.910,43.099 28.910,33.500 C28.910,23.900 3.510,3.499 3.510,3.499 C3.510,3.499 313.507,10.377 638.504,10.377 C963.494,10.377 1273.490,3.499 1273.490,3.499 C1273.490,3.499 1248.092,23.900 1248.092,33.500 Z"/>
                </svg>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 text-center">
                <p class="footer__copyright"><?php echo __('designed') ?>: <a href="http://szkolnastrona.pl/">Szkolnastrona.pl</a></p>
                <br>
                <a href="#top" class="footer__gotop"><?php echo __('go to top'); ?></a>
            </div>
            <div class="col-md-9">
                <?php get_menu_tree('tm', 0, 0, '', true, 'bm', true); ?>
            </div>
        </div>
    </div>
</footer>
<?php
/*
 * Pobranie z zewnątrz
 */
echo get_url_content($external_text['wwwStart'], 'wwwStart', true);
?>
</div>
</body>
</html>