<?php if ($numRowBottomModules > 0): ?>
<div id="modules-bottom" class="modules-content modules-bottom">
    <?php
        $href = '/^<a.*?href=(["\'])(.*?)\1.*$/';
        
        $links = array();
        
        for ($i = 0; $i < $numRowBottomModules; $i++) {
            $tmp = get_module($outRowBottomModules[$i]['mod_name']);
            
            preg_match($href, $tmp, $m);

            if ($m[2] != '') {
                $links[] = $m[2];
            } else {
                $links[] = trans_url_name($outRowBottomModules[$i]['name']);
            }
        }
        
        $modules_color2 = array(
            'mod_forum',
        );
        
        $module_grid_classes = array(1 => "col-sm-12", 2 => "col-sm-6", 3 => "col-sm-4");
        $module_grid_class = array_key_exists($numRowBottomModules, $module_grid_classes) ? $module_grid_classes[$numRowBottomModules] : $module_grid_classes[3];
    ?>
    
    <?php for ($i = 0; $i < $numRowBottomModules; $i++): ?>
    <div class="modules-content__module <?php echo in_array($outRowBottomModules[$i]['mod_name'], $modules_color2) ? 'color2' : ''; ?>" id="<?php echo $outRowBottomModules[$i]['mod_name']; ?>">
        <svg class="modules-content__background" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 278 200" preserveAspectRatio="none">
            <path fill-rule="evenodd" d="M139.290,-0.007 C215.713,-0.007 277.666,44.639 277.666,99.712 C277.666,154.786 215.713,199.432 139.290,199.432 C62.867,199.432 0.914,154.786 0.914,99.712 C0.914,44.639 62.867,-0.007 139.290,-0.007 Z"/>
        </svg>
        <div class="modules-content__content">
            <div class="module__icon modules-content__icon"></div>
            <h2><?php echo $outRowBottomModules[$i]['name'] ?></h2>
            <?php echo get_module($outRowBottomModules[$i]['mod_name']); ?>
        </div>
        <svg class="modules-content__interlude" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 277 55">
            <image x="0px" y="0px" width="277px" height="55px" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAARUAAAA3CAMAAAAVIy5ZAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAACc1BMVEX/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD/fgD///8YAd0kAAAAz3RSTlMAAgQDBQEGBwgJCgsMDg0PEBIRExUWGBkbFB0XHiAiGiQfJSchKSYcKCstLzAyNDYsODUqNzo8Pi5AQkRHSTFLTUUzT0ZDUVNWWFpcXzs9YVVjZWhbXWpebG5xc2J1YD9vd3p8fleBQYOFiIqMTDl2jnmRk5WXmpxInqCjpaeprI+Ha1lKbYmulnuGm7CytLa4u714Upm/jcHDxcfJy83P0NLU1q+dsdhQcqba293f4eLk5ufp6uzt7/Dx8/T19vj5+vv8/f6oi7q+wMLE2cbBty5nAAAAAWJLR0TQDtbPnAAAAAd0SU1FB+EDBw8OHEplFhIAAASYSURBVGjeY2BgYGRiZhgFUMDEAgkMVubRQEECTEysIIqRmXWgXTKYADTnMDIOtEMGF2AGpRJWxtGkggJYmRlBoTLQzhhsAJR5WEeTChpgBOae0UBBB6yjoYINjIYKNjAaKtjAaKhgA6OhggWAS9vRYEEDo6GCDYBbcaMtflQAbfGPNvlRALh3ODqSgApYISMJo6NOKICJCZJ3mEdHKBEANkIJTDMso0ULFDCyscDKE0YWttGiBQwY2dkQCYSZbTRYQICRg50JicvEzjGaiRgYOTlZUARYOLmYyDRr2AAmbi42NCEWLm72gXbWwAJOHm42DEEmbh6uEVy4sHLx8rBgEWfm5uVjIdm0YQLYePn5sLfaWLn4BXhGZHJh5REU4MbpczZ+QSHOgXYi/QGXsLAQvkKVkU9YRIBjoF1JX8ApKCrCR6BdwiYoKiY4gsKFQ1hcTJiI2pdTVFxCko2wuuEA2EWkJKSJq3pZecSlZERGQHrhEJWVkeAjun5hFJCSlZPnG2hX0xbwSijIKQqR1NFhFpJVUJIRGbYDL8yiispKKoIk+4+VX1FZVU19WFbUXFIamqoq/OS1zbikNLW0FcSGWcnLJqamo60lw02BCeKaOrp6ytLDJicxiWno6+kaSFAY06y8Krr6hkaqkgPtH2oAEWMTU0M9OTKzDipgFNYwNTM3NZAf0lmJTd3CzNLcTFWSeumeRV7b0sraxkRJaKA9Rx4QVDO0tbG2spOg9qAAm7iFta29g6OT+BAbtGOSN7JxdrB30aFRWmeVVHV1dnP3sNZRHyLjduzqdo6eHu5ujsaSNB0i4VIyc/f08vZx81XhGmg/E3CpnJ+7v4+3V4C5Gl1cKmJs6ekfGBQc4qhN9YxKFcAioeMSGhYcFOjlqilCT4sFNK3DwyIio6JjHEwUB1FPkkPRxDkwNjoqMiLOxUBgIFzArxzvkRCbmJSckpAar0HXSMEGRFQtPaPSUpKTEqM80zX4B9QtwsauoYlpGZlZ2TnRMbmGMgPQqGFTzHMIzC/Iyc7KzCj0cdQcLC1OQS2XuNicgqLiktKynHx/V1MVukQVv1yedUxiQXlZaUlxUUGif4WW4ECHBCaQ0LEJLSwpr6yqrqmtq29obGq21VShhUUqBi1erW1F7fV1tTXVVZUdneEVOhID7Xv8QEjVMq6pq7u+vae3r3/CxEn9ZaWJk1PdXFSNBSgxVsDY2GaKZ+HkqeUTpk2aOKG/r7envbIx0j9ddUi1txVVbUJap5dNmjZj5qzZc+bOm79g4aLFve1LljZmZYV4e4eG62rr6OjYAYEssjZZXZAQUEY3PDzU2zs7K6ty2ZJlyxcvWrhg/ry5c2bPmjljWnlDpHeFseJA+5ASwKls4LxiZVLjqlUzFy1evnrN2nXrN2zctHnL1m3bd+zctXvP3n37Dxw8dPjI0fNHjxw+dPDA/n179+zetXPH9m1bt2zetHHD+nVr16xevnjWsVVZndErVhgoD7+BMDUDg/RmL6+27OM5OSdOTjs14/TMM7OWI4fK2XOzzsw8PePUtIkFOTnHs0O8vKwMDFTp7EwAVezyxRwAeDsAAAAASUVORK5CYII=" />
        </svg>
    </div>
<?php endfor; ?>
</div>
<?php endif; ?>
