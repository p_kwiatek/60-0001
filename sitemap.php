<h2 class="content-header"><span><?php echo $pageName; ?></span></h2>
<div class="main-text">
    <?php
    foreach ($menuType as $k) {
        if ($k['active'] == 1) {
            ?>
            <h3 class="sub-header"><?php echo $k['name'] ?></h3>
            <?php
            get_menu_tree($k['menutype'], 0, 0, 'sitemap');
        }
    }
    ?>
</div>