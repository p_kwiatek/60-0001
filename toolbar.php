<div id="toolbar" class="toolbar">
    <div class="container">
        <div class="row">
            <div class="col-md-12 clearfix">
                <ul class="toolbar__elements toolbar__elements--left">
                    <li class="home">
                        <a href="index.php" title="<?php echo __("home page"); ?>">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="15.656" height="14.875" viewBox="0 0 15.656 14.875">
                                <defs>
                                    <filter id="home-filter" x="431.625" y="15.938" width="15.656" height="10.063" filterUnits="userSpaceOnUse">
                                        <feOffset result="offset" dy="1" in="SourceAlpha"/>
                                        <feGaussianBlur result="blur"/>
                                        <feFlood result="flood" flood-opacity="0.56"/>
                                        <feComposite result="composite" operator="in" in2="blur"/>
                                        <feBlend result="blend" in="SourceGraphic" in2="SourceGraphic" />
                                    </filter>
                                    <filter id="home-filter-2" x="434.813" y="17.563" width="1.468" height="2.469" filterUnits="userSpaceOnUse">
                                        <feGaussianBlur result="blur" stdDeviation="4" in="SourceAlpha"/>
                                        <feFlood result="flood" flood-opacity="0.58"/>
                                        <feComposite result="composite" operator="out" in2="blur"/>
                                        <feOffset result="offset" dx="2.5" dy="4.33"/>
                                        <feComposite result="composite-2" operator="in" in2="SourceAlpha"/>
                                        <feBlend result="blend" mode="multiply" in2="SourceGraphic"/>
                                    </filter>
                                    <filter id="home-filter-3" x="434.281" y="16.313" width="2.532" height="1.281" filterUnits="userSpaceOnUse">
                                        <feGaussianBlur result="blur" stdDeviation="3" in="SourceAlpha"/>
                                        <feFlood result="flood" flood-opacity="0"/>
                                        <feComposite result="composite" operator="out" in2="blur"/>
                                        <feOffset result="offset" dx="2.5" dy="4.33"/>
                                        <feComposite result="composite-2" operator="in" in2="SourceAlpha"/>
                                        <feBlend result="blend" mode="multiply" in2="SourceGraphic"/>
                                    </filter>
                                </defs>
                                <path class="toolbar__home--path1" d="M445.331,23.527v6.333a8.123,8.123,0,0,1-4.26.9V26.5a1.645,1.645,0,0,0-1.885-1.282,1.558,1.558,0,0,0-1.161,1.188l-0.007.019v4.3s-3.054,0-4.411-.943V23.64l5.014-5.277a1.294,1.294,0,0,1,1.7.113Z" transform="translate(-431.625 -15.938)"/>
                                <path class="toolbar__home--path2" d="M442.02,26.235v4.524a31.059,31.059,0,0,1-5.225-.075V26.348a3.352,3.352,0,0,1,2.531-1.282A3.015,3.015,0,0,1,442.02,26.235Z" transform="translate(-431.625 -15.938)"/>
                                <path class="toolbar__home--path3" d="M445.331,23.078v6.333s-0.338.867-3.353,0.9v-4.26a3.441,3.441,0,0,0-2.542-1.282s-1.834.021-2.442,1.188a0.11,0.11,0,0,0-.007.018h0v4.3a6.84,6.84,0,0,1-3.38-.942V23.191l5.014-5.277a1.294,1.294,0,0,1,1.7.113Z" transform="translate(-431.625 -15.938)"/>
                                <path class="toolbar__home--path4" filter="url(#home-filter)"  d="M440.412,16.29l6.785,6.842s0.509,1.583-.905,1.7a2.087,2.087,0,0,1-.9-0.226l-4.919-4.863a1.631,1.631,0,0,0-2.092,0L433.4,24.885a1.484,1.484,0,0,1-1.7-.509,1.226,1.226,0,0,1,.056-1.018l3.506-3.788,3.336-3.223A1.312,1.312,0,0,1,440.412,16.29Z" transform="translate(-431.625 -15.938)"/>
                                <path class="toolbar__home--path4" filter="url(#home-filter-2)" d="M434.814,17.553h1.47v0.98l-1.47,1.508V17.553Z" transform="translate(-431.625 -15.938)"/>
                                <path class="toolbar__home--path4" filter="url(#home-filter-3)" d="M434.738,16.309h1.621a0.452,0.452,0,0,1,.452.452v0.377a0.452,0.452,0,0,1-.452.452h-1.621a0.452,0.452,0,0,1-.452-0.452V16.761A0.452,0.452,0,0,1,434.738,16.309Z" transform="translate(-431.625 -15.938)"/>
                            </svg>
                            <span><?php echo __("home page"); ?></span>
                        </a>
                    </li>
                    <li class="sitemap">
                        <a href="mapa_strony" title="<?php echo __("sitemap"); ?>">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="19" height="13" viewBox="0 0 19 13">
                                <defs>
                                    <filter id="sitemap-filter" x="0" y="5" width="9" height="8" filterUnits="userSpaceOnUse">
                                        <feOffset result="offset" dy="1" in="SourceAlpha"/>
                                        <feGaussianBlur result="blur" stdDeviation="1.414"/>
                                        <feFlood result="flood" flood-color="#333" flood-opacity="0.47"/>
                                        <feComposite result="composite" operator="in" in2="blur"/>
                                        <feBlend result="blend" in="SourceGraphic" in2="SourceGraphic" />
                                    </filter>
                                    <filter id="sitemap-filter-2" x="10" y="5" width="9" height="8" filterUnits="userSpaceOnUse">
                                        <feOffset result="offset" dy="1" in="SourceAlpha"/>
                                        <feGaussianBlur result="blur" stdDeviation="1.414"/>
                                        <feFlood result="flood" flood-color="#333" flood-opacity="0.47"/>
                                        <feComposite result="composite" operator="in" in2="blur"/>
                                        <feBlend result="blend" in="SourceGraphic" in2="SourceGraphic" />
                                    </filter>
                                    <filter id="sitemap-filter-3" x="5" y="5" width="9" height="8" filterUnits="userSpaceOnUse">
                                        <feOffset result="offset" dy="1" in="SourceAlpha"/>
                                        <feGaussianBlur result="blur" stdDeviation="1.414"/>
                                        <feFlood result="flood" flood-color="#333" flood-opacity="0.47"/>
                                        <feComposite result="composite" operator="in" in2="blur"/>
                                        <feBlend result="blend" in="SourceGraphic" in2="SourceGraphic" />
                                    </filter>
                                    <filter id="sitemap-filter-4" x="4" y="0" width="10" height="8" filterUnits="userSpaceOnUse">
                                        <feOffset result="offset" dy="1" in="SourceAlpha"/>
                                        <feGaussianBlur result="blur" stdDeviation="1.414"/>
                                        <feFlood result="flood" flood-color="#333" flood-opacity="0.47"/>
                                        <feComposite result="composite" operator="in" in2="blur"/>
                                        <feBlend result="blend" in="SourceGraphic" in2="SourceGraphic" />
                                    </filter>
                                </defs>
                                <rect class="toolbar__sitemap--path1" filter="url(#sitemap-filter)" x="1" y="5" width="5.031" height="5" rx="2" ry="2"/>
                                <rect class="toolbar__sitemap--path1" filter="url(#sitemap-filter-2)" x="11" y="5" width="5.031" height="5" rx="2" ry="2"/>
                                <rect class="toolbar__sitemap--path1" filter="url(#sitemap-filter-3)" x="6" y="5" width="5.031" height="5" rx="2" ry="2"/>
                                <rect class="toolbar__sitemap--path2" filter="url(#sitemap-filter-4)" x="5" width="6.938" height="5" rx="2" ry="2"/>
                            </svg>
                            <span><?php echo __('sitemap')?></span>
                        </a>
                    </li>
                    <li class="bip">
                        <a href="<?php echo $pageInfo['bip'] ?>">
                            <svg xmlns="http://www.w3.org/2000/svg" width="39.844" height="17.063" viewBox="0 0 39.844 17.063">
                                <path class="toolbar__bip--path1" d="M701.72,18.993l9.285,9.427V18.993H701.72Zm34.73-.006a5.107,5.107,0,0,0-5.1,5.1v7.126a1.246,1.246,0,1,0,2.492,0V28.451a5.043,5.043,0,0,0,2.611.742,5.1,5.1,0,0,0,0-10.206m0,7.713a2.61,2.61,0,1,1,2.611-2.611A2.613,2.613,0,0,1,736.45,26.7m-16.705-7.707a5.047,5.047,0,0,0-2.611.742V16.664a1.246,1.246,0,0,0-2.492,0V24.1s0,0.005,0,.009a5.1,5.1,0,1,0,5.1-5.112m0,7.714a2.611,2.611,0,1,1,2.611-2.611,2.613,2.613,0,0,1-2.611,2.611m9.591,0.126c-0.168-.083-0.678-0.342-0.678-2.323V20.249a1.246,1.246,0,1,0-2.491,0V24.51c0,1.509.212,3.624,2.048,4.551a1.247,1.247,0,0,0,1.121-2.228m-1.923-8.668a1.371,1.371,0,1,0-1.371-1.369,1.37,1.37,0,0,0,1.371,1.369" transform="translate(-701.719 -15.406)"/>
                            </svg>
                            <span class="sr-only"><?php echo __('bip'); ?></span>
                        </a>
                    </li>
                </ul>
                <ul class="toolbar__elements toolbar__elements--right">
                    <li class="fontsize fontsize--label"><?php echo __('font size'); ?>:</li>
                    <li class="fontsize fontsize--default">
                        <a href="ch_style.php?style=0">
                            <svg xmlns="http://www.w3.org/2000/svg" width="10.28" height="12" viewBox="0 0 10.28 12">
                                <path class="toolbar__font--path1" d="M1209.9,29.205h2.45l-4.28-11.985h-1.72l-4.29,11.985h2.33l0.9-2.635h3.66Zm-3.14-7.14,0.35-1.887h0.09l0.36,1.87,0.88,2.652h-2.55Z" transform="translate(-1202.06 -17.219)"/>
                            </svg>
                            <span class="sr-only"><?php echo __('font default')?></span>
                        </a>
                    </li>
                    <li class="fontsize fontsize--bigger">
                        <a href="ch_style.php?style=r1">
                            <svg xmlns="http://www.w3.org/2000/svg" width="14.94" height="14.219" viewBox="0 0 14.94 14.219">
                                <path class="toolbar__font--path1" d="M1231.28,30.2h2.88l-5.04-14.1h-2.02l-5.04,14.1h2.74l1.06-3.1h4.3Zm-3.7-8.4,0.42-2.22h0.1l0.42,2.2,1.04,3.12h-3ZM1237,19h-2v2h-1V19h-2V18h2V16h1v2h2v1Z" transform="translate(-1222.06 -16)"/>
                            </svg>
                            <span class="sr-only"><?php echo __('font bigger')?></span>
                        </a>
                    </li>
                    <li class="fontsize fontsize--big">
                        <a href="ch_style.php?style=r2">
                            <svg xmlns="http://www.w3.org/2000/svg" width="22.94" height="16.219" viewBox="0 0 22.94 16.219">
                                <path class="toolbar__font--path1" d="M1254.67,31.205h3.31l-5.8-16.215h-2.32l-5.8,16.215h3.16l1.21-3.565h4.95Zm-4.26-9.66,0.49-2.553h0.11l0.48,2.53,1.2,3.588h-3.45ZM1261,19h-2v2h-1V19h-2V18h2V16h1v2h2v1Zm6,0h-2v2h-1V19h-2V18h2V16h1v2h2v1Z" transform="translate(-1244.06 -15)"/>
                            </svg>
                            <span class="sr-only"><?php echo __('font biggest')?></span>
                        </a>
                    </li>
                    <?php
                        if ($_SESSION['contr'] == 0) {
                            $set_contrast = 1;
                            $contrast_txt = __('contrast version');
                        } else {
                            $set_contrast = 0;
                            $contrast_txt = __('graphic version');
                        }
                    ?>
                    <li class="contrast">
                        <span class="sr-only"><?php echo __('font contrast'); ?>:</span>
                        <a href="ch_style.php?contr=<?php echo $set_contrast ?>">
                            <span class="sr-only"><?php echo $contrast_txt?></span>
                        </a>
                    </li>
                    <li id="searchbar" class="searchbar">
                        <a id="search" tabindex="-1" style="display: inline;"></a>
                        <h2 class="sr-only"><?php echo __('search')?></h2>
                        <form id="searchForm" name="f_szukaj" method="get" action="index.php">
                            <input name="c" type="hidden" value="search" />
                            <label for="kword"><span class="sr-only"><?php echo __('search query')?></span></label>
                            <input type="text" id="kword" name="kword" value="<?php echo __('search query'); ?>" onfocus="if (this.value=='<?php echo __('search query'); ?>') {this.value=''};" onblur="if (this.value=='') {this.value='<?php echo __('search query'); ?>'};"/>
                            <button type="submit" name="search"><?php echo __('search action'); ?></button>
                        </form>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
