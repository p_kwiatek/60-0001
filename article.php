<h2 class="content-header"><span><?php echo $pageName?></span></h2>
<div class="main-text">
<?php
echo $message;

if ($showLoginForm)
{
    include( CMS_TEMPL . DS . 'form_login.php');
}
		
if ($showArticle)
{
    echo '<div class="leadArticle">' . $article['lead_text'] . '</div>';
    
    echo $article['text'];
		
    if (! check_html_text($article['author'], '') )
    {
	?>
	<p class="author-name"><?php echo __('author')?>: <?php echo $article['author']?></p>
	<?php
    }
    
    ?>
</div>
<div>
    <?php
    /*
     *  Wypisanie plikow do pobrania
     */
    if ($numFiles > 0)
    {
	?>
	<div class="files-wrapper row">
            <div class="col-xs-12">
                <h3 class="files-header"><?php echo __('files')?></h3>
                <ul class="list-unstyled">
                <?php
                foreach ($outRowFiles as $row)
                {
                    $target = 'target="_blank" ';

                    if (filesize('download/'.$row['file']) > 5000000)
                    {
                        $url = 'download/'.$row['file'];
                    } else
                    {
                        $url = 'index.php?c=getfile&amp;id='.$row['id_file'];
                    }
                    if (trim($row['name']) == '')
                    {
                        $name = $row['file'];
                    } else
                    {
                        $name = $row['name'];
                    }			
                    $size = file_size('download/'.$row['file']);	
                    ?>
                    <li>
                        <a href="<?php echo $url?>" <?php echo $target?>>
                            <i class="icon-doc-text-inv icon" aria-hidden="true"></i>
                            <span class="title">
                                <?php echo $name?>
                                <span class="size">(<?php echo $size?>)</span>
                            </span>
                        </a>
                    </li>
                    <?php
                }
                ?>
                </ul>
            </div>
	</div>
    <?php
    }
		
    /*
     *  Wypisanie zdjec
     */
    if ($numPhotos > 0)
    {	
	$i = 0;
	?>
	<div class="gallery-wrapper row">
            <div class="col-xs-12">
                <h3 class="gallery-header"><?php echo __('gallery')?></h3>
                <ul class="list-unstyled gallery">
                <?php
                foreach ($outRowPhotos as $row)
                {
                    $i++;
                    $noMargin = '';
                    if ($i == $pageConfig['zawijaj'])
                    {
                        $noMargin = ' noMargin';
                    }
                    ?>
                    <li>
                        <a href="files/<?php echo $lang?>/<?php echo $row['file']?>" title="<?php echo __('enlarge image') . ': ' . $row['name']?>" data-fancybox-group="gallery" class="photo fancybox">
                            <svg class="photo-wrapper__hover" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="36px" height="38px">
                                <defs>
                                    <filter filterUnits="userSpaceOnUse" id="<?php echo 'gallery-image-hover-filter-' . $i; ?>" x="0px" y="0px" width="36px" height="38px"  >
                                        <feOffset in="SourceAlpha" dx="0" dy="2" />
                                        <feGaussianBlur result="blurOut" stdDeviation="0" />
                                        <feFlood flood-color="rgb(0, 0, 0)" result="floodOut" />
                                        <feComposite operator="atop" in="floodOut" in2="blurOut" />
                                        <feComponentTransfer><feFuncA type="linear" slope="0.2"/></feComponentTransfer>
                                        <feMerge>
                                            <feMergeNode/>
                                            <feMergeNode in="SourceGraphic"/>
                                        </feMerge>
                                    </filter>
                                </defs>
                                <g filter="<?php echo 'url(#gallery-image-hover-filter-' . $i .')'; ?>">
                                    <path fill-rule="evenodd" d="M35.109,32.611 L32.611,35.108 C32.151,35.568 31.406,35.568 30.946,35.108 L25.952,30.114 C25.492,29.654 25.492,28.909 25.952,28.449 L26.086,28.314 L23.738,25.966 C23.679,25.906 23.646,25.833 23.604,25.764 C21.168,27.727 18.075,28.907 14.703,28.907 C6.859,28.907 0.500,22.548 0.500,14.703 C0.500,6.859 6.859,0.500 14.703,0.500 C22.548,0.500 28.907,6.859 28.907,14.703 C28.907,18.075 27.727,21.168 25.764,23.604 C25.833,23.646 25.906,23.679 25.966,23.738 L28.314,26.086 L28.449,25.952 C28.909,25.492 29.654,25.492 30.114,25.952 L35.109,30.946 C35.568,31.406 35.568,32.151 35.109,32.611 ZM14.703,5.971 C9.880,5.971 5.970,9.880 5.970,14.703 C5.970,19.526 9.880,23.436 14.703,23.436 C19.526,23.436 23.436,19.526 23.436,14.703 C23.436,9.880 19.526,5.971 14.703,5.971 Z"/>
                                </g>
                            </svg>
                            <img src="files/<?php echo $lang?>/mini/<?php echo $row['file']?>" alt="" />
                            <?php
                            if (! check_html_text($row['name'], '') )
                            {
                                ?>
                                <p class="photo-name" aria-hidden="true"><?php echo $row['name']?></p>
                                <?php
                            }
                            ?>
                        </a>
                    </li>
                <?php
                }
                ?>
                </ul>
            </div>
	</div>
    <?php
    }		
    if ($outSettings['pluginTweet'] == 'włącz')
    {
	 echo '<div class="Tweet"><iframe frameborder="0" scrolling="no" src="//platform.twitter.com/widgets/tweet_button.html" style="width:80px; height:30px;"></iframe></div>';  
    }

    if ($outSettings['pluginFB'] == 'włącz')
    {
        $color = 'light';
        if ($_SESSION['contr'] == 1)
        {
            $color = 'dark';
        }        
	$fb_url = urlencode('http://'.$pageInfo['host'].'/index.php?c=article&amp&id='. $_GET['id']);
	echo '<div class="FBLike"><iframe src=\'http://www.facebook.com/plugins/like.php?href='.$fb_url.'&amp;layout=standard&amp;show_faces=true&amp;width=400&amp;action=like&amp;font=tahoma&amp;colorscheme='.$color.'&amp;height=32&amp;show_faces=false\' scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:400px; height:32px;"></iframe></div>';   
    }
}
?>
</div>