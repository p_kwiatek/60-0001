<h2 class="content-header"><span><?php echo __('login action'); ?></span></h2>
<div class="main-text">
    <?php
    echo $message;

    if (!$showProtected) {
        include_once ( CMS_TEMPL . DS . 'form_login.php');
    }
    ?>	
</div>