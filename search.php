<h2 class="content-header"><span><?php echo $pageName; ?></span></h2>
<div class="main-text">
    <?php
    echo $message;
    ?>	
    <div class="search-list">
        <?php
        for ($i = $searchStart; $i < ($searchStart + $pageConfig['limit']); $i++) {
            ?>
            <div class="search-text">
                <h3 class="sub-header"><?php echo $searchArray[$i]['url'] ?></h3>
                <div class="search-lead"><?php echo $searchArray[$i]['lead'] ?></div>
            </div>
            <?php
        }
        ?>	
    </div>
    <?php
    $url = $PHP_SELF . '?c=' . $_GET['c'] . '&amp;kword=' . $_GET['kword'] . '&amp;s=';
    include (CMS_TEMPL . DS . 'pagination.php');
    ?>
</div>