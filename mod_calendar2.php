<div class="main-text">
<h2 class="content-header"><span><?php echo $pageName; ?></span></h2>
<?php
// Wypisanie artykulow
if ($numArticles > 0)
{	
    $i = 0;
    ?>
    <div class="article-wrapper">
    <h2 class="sr-only"><?php echo __('articles')?></h2>
    <?php
    foreach ($outArticles as $row)
    {
	$highlight = $url = $target = $url_title = $protect = '';
			
	if ($row['protected'] == 1)
	{
	    $protect = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 7 10">
<defs><filter filterUnits="userSpaceOnUse" id="article-lock-' . $i . '" x="0px" y="0px" width="7px" height="10px"><feOffset in="SourceAlpha" dx="0" dy="1" /><feGaussianBlur result="blurOut" stdDeviation="0" /><feFlood flood-color="rgb(255, 189, 0)" result="floodOut" /><feComposite operator="atop" in="floodOut" in2="blurOut" /><feComponentTransfer><feFuncA type="linear" slope="1"/></feComponentTransfer><feMerge><feMergeNode/><feMergeNode in="SourceGraphic"/></feMerge></filter></defs><g filter="url(#article-lock- ' . $i . ')"><path fill-rule="evenodd" d="M6.124,9.000 L0.867,9.000 C0.383,9.000 -0.010,8.622 -0.010,8.156 L-0.010,4.781 C-0.010,4.315 0.383,3.937 0.867,3.937 L1.159,3.937 L1.159,2.250 C1.159,1.007 2.205,-0.000 3.495,-0.000 C4.786,-0.000 5.832,1.007 5.832,2.250 L5.832,3.920 L4.663,3.920 L4.663,1.969 C4.663,1.503 4.140,1.125 3.495,1.125 C2.850,1.125 2.327,1.503 2.327,1.969 L2.327,3.937 L6.124,3.937 C6.608,3.937 7.000,4.315 7.000,4.781 L7.000,8.156 C7.000,8.622 6.608,9.000 6.124,9.000 ZM3.495,5.062 C2.850,5.062 2.327,5.566 2.327,6.187 C2.327,6.599 2.568,6.944 2.911,7.140 L2.911,7.857 C2.911,8.168 3.173,8.420 3.495,8.420 C3.818,8.420 4.079,8.168 4.079,7.857 L4.079,7.140 C4.422,6.944 4.663,6.599 4.663,6.187 C4.663,5.566 4.140,5.062 3.495,5.062 Z"/></g></svg>';
	    $url_title = ' title="' . __('page requires login') . '"';
	}				
			
	if (trim($row['ext_url']) != '')
	{
	    if ($row['new_window'] == '1')
	    {
		$target = ' target="_blank"';
	    }	
	    $url_title = ' title="' . __('opens in new window') . '"';
	    $url = ref_replace($row['ext_url']);					
	} else
	{
	    if ($row['url_name'] != '')
	    {
		$url = 'a,' . $row['id_art'] . ',' . $row['url_name'];
	    } else
	    {
		$url = 'index.php?c=article&amp;id=' . $row['id_art'];
	    }
	}	
									
	$margin = ' no-photo';
	if (is_array($photoLead[$row['id_art']]))
	{
	    $margin = '';
	}			
			
	$row['show_date'] = substr($row['show_date'], 0, 10);
        
        $highlight = '';
        if ($row['highlight'] == 1)
        {
            $highlight = ' highlight-article';
        }        
	?>
        <div class="article<?php echo $highlight?><?php if (!is_array($photoLead[$row['id_art']])): ?> no-photo<?php endif; ?>">
            <?php
            if (is_array($photoLead[$row['id_art']]))
            {
                $photo = $photoLead[$row['id_art']];
                ?>
                <div class="photo-wrapper<?php echo $photoWrapper; ?>">
                    <a href="files/<?php echo $lang?>/<?php echo $photo['file']?>" title="<?php echo __('enlarge image') . ': ' . $row['name']?>" class="photo fancybox" data-fancybox-group="gallery">
                        <span class="sr-only"><?php echo __('enlarge image')?>: <?php echo $row['name']?></span>
                        <svg class="photo-wrapper__hover" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="36px" height="38px">
                            <defs>
                                <filter filterUnits="userSpaceOnUse" id="<?php echo 'article-image-hover-filter-' . $i; ?>" x="0px" y="0px" width="36px" height="38px"  >
                                    <feOffset in="SourceAlpha" dx="0" dy="2" />
                                    <feGaussianBlur result="blurOut" stdDeviation="0" />
                                    <feFlood flood-color="rgb(0, 0, 0)" result="floodOut" />
                                    <feComposite operator="atop" in="floodOut" in2="blurOut" />
                                    <feComponentTransfer><feFuncA type="linear" slope="0.2"/></feComponentTransfer>
                                    <feMerge>
                                        <feMergeNode/>
                                        <feMergeNode in="SourceGraphic"/>
                                    </feMerge>
                                </filter>
                            </defs>
                            <g filter="<?php echo 'url(#article-image-hover-filter-' . $i .')'; ?>">
                                <path fill-rule="evenodd" d="M35.109,32.611 L32.611,35.108 C32.151,35.568 31.406,35.568 30.946,35.108 L25.952,30.114 C25.492,29.654 25.492,28.909 25.952,28.449 L26.086,28.314 L23.738,25.966 C23.679,25.906 23.646,25.833 23.604,25.764 C21.168,27.727 18.075,28.907 14.703,28.907 C6.859,28.907 0.500,22.548 0.500,14.703 C0.500,6.859 6.859,0.500 14.703,0.500 C22.548,0.500 28.907,6.859 28.907,14.703 C28.907,18.075 27.727,21.168 25.764,23.604 C25.833,23.646 25.906,23.679 25.966,23.738 L28.314,26.086 L28.449,25.952 C28.909,25.492 29.654,25.492 30.114,25.952 L35.109,30.946 C35.568,31.406 35.568,32.151 35.109,32.611 ZM14.703,5.971 C9.880,5.971 5.970,9.880 5.970,14.703 C5.970,19.526 9.880,23.436 14.703,23.436 C19.526,23.436 23.436,19.526 23.436,14.703 C23.436,9.880 19.526,5.971 14.703,5.971 Z"/>
                            </g>
                        </svg>
                        <svg class="photo-wrapper__background" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 267 267">
                            <defs>
                                <filter filterUnits="userSpaceOnUse" id="<?php echo 'article-filter-' . $i; ?>" x="1.5px" y="1.5px" width="264px" height="150%">
                                    <feOffset in="SourceAlpha" dx="0" dy="2" />
                                    <feGaussianBlur result="blurOut" stdDeviation="0" />
                                    <feFlood flood-color="rgb(0, 0, 0)" result="floodOut" />
                                    <feComposite operator="atop" in="floodOut" in2="blurOut" />
                                    <feComponentTransfer><feFuncA type="linear" slope="0.2"/></feComponentTransfer>
                                    <feMerge>
                                        <feMergeNode/>
                                        <feMergeNode in="SourceGraphic"/>
                                    </feMerge>
                                </filter>
                                <pattern id="<?php echo 'article-image-' . $i; ?>" patternUnits="objectBoundingBox" width="100%" height="100%">
                                    <image height="100%" width="100%" preserveAspectRatio="xMidYMid slice" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="files/<?php echo $lang?>/mini/<?php echo $photo['file']; ?>"></image>
                                </pattern>
                            </defs>
                            <g filter="<?php echo 'url(#article-filter-' . $i . ')'; ?>">
                                <path fill-rule="evenodd" stroke-width="3px" fill="<?php echo 'url(#article-image-' . $i . ')'; ?>"
                                d="M133.500,3.500 C205.297,3.500 263.500,61.703 263.500,133.500 C263.500,205.297 205.297,263.500 133.500,263.500 C61.703,263.500 3.500,205.297 3.500,133.500 C3.500,61.703 61.703,3.500 133.500,3.500 Z"/>
                            </g>
                        </svg>
                    </a>
                </div>
                <?php
            }   
            ?>
            <div class="lead-text<?php echo $margin; ?>">
                <h4 class="article-title <?php echo $margin?>">
                    <a href="<?php echo $url?>" <?php echo $url_title . $target ?>><?php echo $row['name'] . $protect?></a>
                </h4>
                <?php if ($row['show_date'] != '' && $row['show_date'] != '0000-00-00') { ?>
                    <div class="article-date">
                        <img src="<?php echo $templateDir . '/images/calendar.png'; ?>" alt="">
                        <?php echo $row['show_date'] ?>
                    </div>
                <?php } ?>
                <div class="lead-main-text">
                    <?php echo truncate_html($row['lead_text'], 300, '...')?>
                </div>
                <div class="article-meta">
                    <a href="<?php echo $url ?>" <?php echo $url_title . $target ?> class="button"><?php echo __('more') ?><span class="sr-only"> <?php echo __('about')?>: <?php echo $row['name']; ?></span></a>
                </div>
            </div>
        </div>
    <?php
    }
    ?>
    </div>
<?php
}
?>
</div>
