<div class="forum-wrapper">
<?php
/**
 * logowanie
 */
if ($loginForm){
    echo '<a name="logowanie" id="logowanie"></a>';
    echo '<h2 class="content-header"><span>' . $pageName . '</span></h2>';
    ?>
    <form name="loginForm" id="loginForm" class="" method="post" action="<?php echo $url; ?>,zaloguj#logowanie">
	<fieldset>
	    <?php
	    echo $message;
	    ?>
	    <legend><?php echo __('login action'); ?></legend>
	    
	    <div class="formL"><label for="login" class="formLabel"><?php echo __('login'); ?>:</label></div>
	    <div class="formR"><input type="text" id="login" name="login" class="inText" size="35" maxlength="50" /><span id="loginError" class="msgMarg"></span></div>
	    
	    
	    <div class="formL"><label for="password" class="formLabel"><?php echo __('password'); ?>:</label></div>
	    <div class="formR"><input type="password" id="password" name="password" class="inText" size="35" maxlength="50" /><span id="passwordError" class="msgMarg"></span></div>
	    
	    
	    <input type="hidden" name="forumLogin" value="1" />
	    
	    <div class="formL"></div>
	    <div class="formR">
                <input type="submit" class="butForm" name="ok" value="<?php echo __('login action'); ?>" />
                <a href="generuj-haslo" class="button"><?php echo __('password forgot'); ?></a>
            </div>
	    
	    
	</fieldset>
	
    </form>

<script type="text/javascript">
    $(document).ready(function() {
	var form = $('#loginForm');
	form.submit(function(){
	    if (validateLogin() && validatePass()){
		//return true;
	    } else {
		return false;
	    }
	});
	$('#login').blur(validateLogin);
	function validateLogin(){
	    var value = $('#login').val();
	   
	    if (value.length < 4){
		$('#login').addClass('inError');
		$('#loginError').addClass('msgError').text('<?php echo __('error min length login'); ?>');
		return false;
	    } else {
		$('#login').removeClass('inError');
		$('#loginError').removeClass('msgError').text('');
		return true;
	    }
	}
	
	$('#password').blur(validatePass);
	function validatePass(){
	    var value = $('#password').val();
	    if (value.length < 8 || value.length > 50){
		$('#password').addClass('inError');
		$('#passwordError').addClass('msgError').text('<?php echo __('error min length password'); ?>');
		return false;
	    } else {
		$('#password').removeClass('inError');
		$('#passwordError').text('');
		return true;
	    } 
	}	
    });
</script>
    <?php
}
/**
 * rejestracja powiodła się
 */
if ($registerSuccess){ ?>
    <h2 class="content-header"><span><?php echo $pageName; ?></span></h2>
    <div class="main-text">
        <?php echo $message; ?>
    </div>

    <div class="row">
        <ul class="list-unstyled list-inline col-xs-12 back-links">
            <li><a href="forum" class="button"><?php echo __('forum home page') ?></a></li>
            <li><a href="index.php" class="button"><?php echo __('home page') ?></a></li>
        </ul>
        <div class="clearfix"></div>
    </div>
<?php
}
/**
 * formularz rejestracji
 */
if ($registerForm){
    echo '<a name="rejestracja" id="rejestracja"></a>';
    echo '<h2 class="content-header"><span>' . $pageName . '</span></h2>';
    ?>
    <form name="registerForm" id="registerForm" class="jNice" method="post" action="<?php echo $url; ?>,rejestruj#rejestracja" enctype="multipart/form-data">
	<fieldset>
	    <?php
	    echo $message;
	    ?>
	    <legend><?php echo __('register action'); ?></legend>
	    
	    <div class="formL"><span class="asterisk">*</span><label for="login" class="formLabel"><?php echo __('login'); ?>:</label></div>
	    <div class="formR"><input type="text" id="login" name="login" class="inText" size="35" maxlength="50" value="<?php echo $login; ?>" /><span id="loginError" class="msgMarg"></span></div>
	    
	    
	    <div class="formL"><span class="asterisk">*</span><label for="password" class="formLabel"><?php echo __('password'); ?>:</label></div>
	    <div class="formR"><input type="password" id="password" name="password" class="inText" size="35" maxlength="50" value="<?php echo $password; ?>" /><span id="passwordError" class="msgMarg"></span></div>
	    
	    
	    <div class="formL"><span class="asterisk">*</span><label for="password_confirm" class="formLabel"><?php echo __('repeat password'); ?>:</label></div>
	    <div class="formR"><input type="password" id="password_confirm" name="password_confirm" class="inText" size="35" maxlength="50" value="<?php echo $password_confirm; ?>" /><span id="passwordConfirmError" class="msgMarg"></span></div>
	    	    
	    
	    <div class="formL"><span class="asterisk">*</span><label for="email" class="formLabel"><?php echo __('email'); ?>:</label></div>
	    <div class="formR"><input type="text" id="email" name="email" class="inText" size="35" maxlength="100" value="<?php echo $email; ?>" /><span id="emailError" class="msgMarg"></span></div>
	    
	    
	    <div class="formL"><label for="first_name" class="formLabel"><?php echo __('firstname'); ?>:</label></div>
	    <div class="formR"><input type="text" id="first_name" name="first_name" class="inText" size="35" maxlength="50" value="<?php echo $first_name; ?>" /></div>
	    
	    
	    <div class="formL"><label for="last_name" class="formLabel"><?php echo __('lastname'); ?>:</label></div>
	    <div class="formR"><input type="text" id="last_name" name="last_name" class="inText" size="35" maxlength="50" value="<?php echo $last_name; ?>" /></div>
	    
	    
            <div class="formL"><span class="formLabel"><?php echo __('gender'); ?>:</span></div>
	    <div class="formR radio">
	    	<div>
				<input type="radio" id="sex_m" name="sex" value="m" checked="checked" />
				<label for="sex_m" id="l_sex_m"><?php echo __('man'); ?></label>
			</div>
			<div>
				<input type="radio" id="sex_f" name="sex" value="f" />
				<label for="sex_f" id="l_sex_f"><?php echo __('woman'); ?></label>
			</div>
	    </div>
	    
	    
	    <div class="formL"><label for="avatar" class="formLabel"><?php echo __('avatar'); ?>:</label></div>
	    <div class="formR">
		
		<input type="text" id="avatar" name="avatar" size="35" readonly="readonly" class="inText" />
		<div class="file_input_div">
			<input id="btnFilePos" class="butForm" type="button" value="<?php echo __('choose'); ?>..." name="fake" />
			<input id="avatar_f" class="avatar_f" type="file" onchange="javascript: document.getElementById('avatar').value = this.value" name="avatar_f" size="36" />
		</div>
		<div class="comment" class="formLabel"><?php echo __('file info'); ?></div>

	    </div>
	    
	    
	    <div class="formL"></div>
	    <div class="formR"><p><?php echo __('math info'); ?></p>		
	    </div>
	    
	    
	    <div class="formL"><label for="captcha" class="formLabel"><span class="asterisk">*</span><?php echo __('math result'); ?>:</label></div>
	    <div class="formR"><label for="captcha" class="captchaTxt"><strong><?php echo $captchaTxt; ?></strong> <?php echo __('is'); ?></label><input type="text" id="captcha" name="captcha" size="2" maxlength="2" class="inTextSmall" /><span id="captchaError" class="msgMarg"></span></div>
	    
		 
	    <div class="formL"><span class="asterisk">*</span><?php echo __('required fields'); ?></div>
	    <div class="formR"><input type="submit" name="ok" value="<?php echo __('register action'); ?>" class="butForm"/></div>
	    	    
	    
	</fieldset>
    </form>

<script type="text/javascript">
    $(document).ready(function() {
	var form = $('#registerForm');
	form.submit(function(){
	    if (validateLogin() && validatePass() && validatePasswords() && validateEmail() && validateCaptcha()){
		//return true;
	    } else {
		return false;
	    }
	});
	
	$('#login').blur(validateLogin);
	function validateLogin(){
	    var value = $('#login').val();
	   
	    if (value.length < 5){
		$('#login').addClass('inError');
		$('#loginError').addClass('msgError').text('<?php echo __('error min length login'); ?>');
		return false;
	    } else {
		$('#login').removeClass('inError');
		$('#loginError').removeClass('msgError').text('');
		return true;
	    }
	}
	
	$('#password').blur(validatePass);
	function validatePass(){
	    var value = $('#password').val();
	    if (value.length < 9 || value.length > 50){
		$('#password').addClass('inError');
		$('#passwordError').addClass('msgError').text('<?php echo __('error min length password'); ?>');
		return false;
	    } else {
		validatePasswords();
		$('#password').removeClass('inError');
		$('#passwordError').removeClass('msgError').text('');
		return true;
	    } 
	}
	
	$('#password_confirm').blur(validatePasswords);
	function validatePasswords(){
	    if ($('#password').val() != $('#password_confirm').val()){
		$('#password_confirm').addClass('inError');
		$('#passwordConfirmError').addClass('msgError').text('<?php echo __('error passwords dont match'); ?>');
		return false;
	    } else {
		$('#password_confirm').removeClass('inError');
		$('#passwordConfirmError').removeClass('msgError').text('');
		return true;
	    }
	}
	
	$('#email').blur(validateEmail);
	function validateEmail(){
	    var exp = /^[a-zA-Z0-9]+[a-zA-Z0-9_.-]+[a-zA-Z0-9_-]+@[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{2,4}$/;
	    var email = $("#email").val();
	    if (!exp.test(email)){
		$('#email').addClass('inError');
		$('#emailError').addClass('msgError').text('<?php echo __('error incorrect email'); ?>');
		return false;
	    } else {
		$('#email').removeClass('inError');
		$('#emailError').removeClass('msgError').text('');
		return true;
	    }
	}
	
	$('#captcha').blur(validateCaptcha);
	function validateCaptcha(){
	    var value = $('#captcha').val();
	    if (value == ''){
		$('#captcha').addClass('inError');
		$('#captchaError').text('<?php echo __('error captcha'); ?>');
		return false;
	    } else {
		$('#captcha').removeClass('inError');
		$('#captchaError').text('');
		return true;
	    }
	}
	
    });
</script>

<?php
}


//formularz dodania wątku
if ($addTopicForm){
    echo '<a name="watki" id="watki"></a>';
    echo '<h2 class="content-header"><span>' . $pageName . '</span></h2>';  
?>

    <form name="formAddTopic" id="formAddTopic" class="" method="post" action="<?php echo $url; ?>,dodaj#watki">
	<fieldset>
	    <?php
	    echo $message;
	    ?>
	    <legend><?php echo __('add new topic'); ?></legend>
	    
	    <div class="txt_com"><?php echo $topicName; ?></div>
	    
	    <?php
	    if ($forumLogged){
	    ?>    
	    <input type="hidden" name="author" value="<?php echo $who; ?>" />
	    
	    <?php
	    } else {
	    ?>
	    
            <div class="formL"><label for="topicAuthor" class="formLabel"><span class="asterisk">*</span><?php echo __('author'); ?>:</label></div>
	    <div class="formR"><input type="text" id="topicAuthor" name="author" class="inText inLong" size="35" maxlength="50" value="<?php echo $author; ?>" /><span id="topicAuthorError" class="msgMarg"></span></div>
	    
	    
	    <?php
	    }
	    ?>
	    
	    <div class="formL"><label for="topicTitle" class="formLabel"><span class="asterisk">*</span><?php echo __('title'); ?>:</label></div>
	    <div class="formR"><input type="text" id="topicTitle" class="inText inLong" name="title" size="35" maxlength="50" value="<?php echo $title; ?>" /><span id="topicTitleError" class="msgMarg"></span></div>
	    
	    
	    <div class="formL"><label for="topicContent" class="formLabel"><span class="asterisk">*</span><?php echo __('content'); ?>:</label></div>
	    <div class="formR"><textarea id="topicContent" name="content" rows="8" cols="40" class="inTextArea inLong"><?php echo $content; ?></textarea><span id="topicContentError" class="msgMarg"></span></div>
	    
	    
	    <div class="formL"></div>
	    <div class="formR"><p><?php echo __('math info'); ?></p>		
	    </div>
	    
	    
	    <input type="hidden" name="id_topic" value="<?php echo $topicId; ?>" />
	    
	    <div class="formL"><label for="topicCaptcha" class="formLabel"><span class="asterisk">*</span><?php echo __('math result'); ?>:</label></div>
	    <div class="formR"><label for="captcha" class="captchaTxt"><strong><?php echo $captchaTxt; ?></strong> <?php echo __('is'); ?></label><input type="text" id="topicCaptcha" name="captcha" size="2" maxlength="2" class="inTextSmall" /><span id="topicCaptchaError" class="msgMarg"></span></div>
	    
		 
	    <div class="formL"><span class="asterisk">*</span><?php echo __('required fields'); ?></div>
	    <div class="formR"><input type="submit" name="ok" value="<?php echo __('add'); ?>" class="butForm"/></div>
	    
	    
	</fieldset>
    
    </form>

    <script type="text/javascript">
            $(document).ready(function() {
                var form = $('#formAddTopic');
                form.submit(function(){
                    if (validateAuthor() && validateTitle() && validateContent() && validateCaptcha()){
                        //return true;
                    } else {
                       return false;
                    }
                });

                $('#topicAuthor').blur(validateAuthor);
                function validateAuthor(){
                    var value = $('#topicAuthor').val();
                    if (value == ''){
                        $('#topicAuthor').addClass('inError');
                        $('#topicAuthorError').addClass('msgError').text('<?php echo __('error topic author'); ?>');
                        return false;
                    } else {
                        $('#topicAuthor').removeClass('inError');
                        $('#topicAuthorError').text('');
                        return true;
                    }
                }

                $('#topicTitle').blur(validateTitle);
                function validateTitle(){
                    var value = $('#topicTitle').val();
                    if (value == ''){
                        $('#topicTitle').addClass('inError');
                        $('#topicTitleError').addClass('msgError').text('<?php echo __('error topic title'); ?>');
                        return false;
                    } else {
                        $('#topicTitle').removeClass('inError');
                        $('#topicTitleError').text('');
                        return true;
                    }
                }

                $('#topicContent').blur(validateContent);
                function validateContent(){
                    var value = $('#topicContent').val();
                    if (value == ''){
                        $('#topicContent').addClass('inError');
                        $('#topicContentError').addClass('msgError').text('<?php echo __('error topic content'); ?>');
                        return false;
                    } else {
                        $('#topicContent').removeClass('inError');
                        $('#topicContentError').text('');
                        return true;
                    }
                }

                $('#topicCaptcha').blur(validateCaptcha);
                function validateCaptcha(){
                    var value = $('#topicCaptcha').val();
                    if (value == ''){
                        $('#topicCaptcha').addClass('inError');
                        $('#topicCaptchaError').text('<?php echo __('error captcha'); ?>');
                        return false;
                    } else {
                        $('#topicCaptcha').removeClass('inError');
                        $('#topicCaptchaError').text('');
                        return true;
                    }
                }

            });
    </script>   
    <?php
}

//formularz dodania odpowiedzi
if ($addRespondForm){
    echo '<a name="odpowiedz" id="odpowiedz"></a>';
    
    echo '<h2 class="content-header"><span>' . $pageName . '</span></h2>';
    
    ?>
    <form name="formAddPost" id="formAddPost" class="" method="post" action="<?php echo $url; ?>,odpowiedz#odpowiedzi">
	<fieldset>
	<?php
	echo $message;
	?>
	    <legend><?php echo __('add new post'); ?></legend>
	    
	    <div class="txt_com"><?php echo $topicName; ?></div>
	    
	    <?php
	    if ($forumLogged){
	    ?>
	    <input type="hidden" name="author" value="<?php echo $who; ?>" />
	    <?php
	    } else {
	    ?>
	    <div class="formL"><label for="postAuthor" class="formLabel"><span class="asterisk">*</span><?php echo __('author'); ?>:</label></div>
	    <div class="formR"><input type="text" id="postAuthor" name="author" class="inText inLong" size="35" maxlength="50" value="<?php echo $author; ?>" /><span id="postAuthorError" class="msgMarg"></span></div>
	    
	    <?php
	    }
	    ?>
	    
	    <div class="formL"><label for="postContent" class="formLabel"><span class="asterisk">*</span><?php echo __('content'); ?>:</label></div>
	    <div class="formR"><textarea id="postContent" name="content" rows="8" cols="40" class="inTextArea inLong"><?php echo $cita . $content; ?></textarea><span id="postContentError" class="msgMarg"></span></div>
	    
	    
	    <div class="formL"></div>
	    <div class="formR"><p><?php echo __('math info'); ?></p>		
	    </div>
	    
	    
	    <input type="hidden" name="id_topic" value="<?php echo $topicId; ?>" />
	    
	    <div class="formL"><label for="postCaptcha" class="formLabel"><span class="asterisk">*</span><?php echo __('math result'); ?>:</label></div>
	    <div class="formR"><label for="captcha" class="captchaTxt"><strong><?php echo $captchaTxt; ?></strong> <?php echo __('is'); ?></label><input type="text" id="postCaptcha" name="captcha" size="2" maxlength="2" class="inTextSmall" /><span id="postCaptchaError" class="msgMarg"></span></div>
	    
		 
	    <div class="formL"><span class="asterisk">*</span><?php echo __('required fields'); ?></div>
	    <div class="formR"><input type="submit" name="ok" value="<?php echo __('respond action'); ?>" class="butForm"/></div>
	    
	    
	</fieldset>

    </form>


<script type="text/javascript">
	$(document).ready(function() {
	    var form = $('#formAddPost');
	    form.submit(function(){
		if (validateAuthor() && validateContent() && validateCaptcha()){
		    //return true;
		} else {
		    return false;
		}
	    });
	    
	    $('#postAuthor').blur(validateAuthor);
	    function validateAuthor(){
		var value = $('#postAuthor').val();
		if (value == ''){
		    $('#postAuthor').addClass('inError');
		    $('#postAuthorError').addClass('msgError').text('<?php echo __('error post author'); ?>');
		    return false;
		} else {
		    $('#postAuthor').removeClass('inError');
		    $('#postAuthorError').text('');
		    return true;
		}
	    }
	    
	    $('#postContent').blur(validateContent);
	    function validateContent(){
		var value = $('#postContent').val();
		if (value == ''){
		    $('#postContent').addClass('inError');
		    $('#postContentError').addClass('msgError').text('<?php echo __('error post content'); ?>');
		    return false;
		} else {
		    $('#postContent').removeClass('inError');
		    $('#postContentError').text('');
		    return true;
		}
	    }

	    $('#postCaptcha').blur(validateCaptcha);
	    function validateCaptcha(){
		var value = $('#postCaptcha').val();
		if (value == ''){
		    $('#postCaptcha').addClass('inError');
		    $('#postCaptchaError').text('<?php echo __('error captcha'); ?>');
		    return false;
		} else {
		    $('#postCaptcha').removeClass('inError');
		    $('#postCaptchaError').text('');
		    return true;
		}
	    }
	    
	});
</script>

    <?php
}


if ($showTopics){

    echo '<h2 class="content-header"><span>' . $pageName . '</span></h2>';
    echo $message;
    
    if (!$forumLogged){
	?>
        <div class="clearfix">
            <ul id="forumActions">
                <li><a href="forum,logowanie" class="button"><?php echo __('login action'); ?></a></li>
                <li><a href="forum,rejestracja" class="button"><?php echo __('register action'); ?></a></li>
            </ul>
        </div>
	<?php
    }
    
    
    if ($topicId != 0){
	echo '<div class="topicText clearfix">';
	echo '<span class="topicDate">' . $topicContent['date'] . '</span>';
	echo '<span class="topicHour">' . $topicContent['hour'] . '</span>';		
	echo '<div class="topicAuthor">';
	echo '<img class="forumAvatarImage" src="' . $topicContent['avatar'] . '" alt="' . $topicContent['alt'] . '" />';
	echo '<span class="author">' . $topicContent['author'] . '</span>';
	echo '</div>';
	
	echo '<div class="topicContent">' . nl2br($topicContent['content']) . '</div>';
	echo '</div>';
	
	if ($addTopic){
	    echo '<ul class="respondButtonList clearfix">';
	    echo '<li><a href="forum,o,' . $topicId . ',0,' . $urlAddPost . '#odpowiedz" class="button">' . __('respond action') . '</a></li>';
	    echo '<li><a href="#odpowiedzi" class="button">' . __('show responds') . '</a>';
	    echo '</ul>';
	}
    }
    ?>
        
    <div class="forum-threads-wrapper">

    <a name="watki" id="watki"></a>
    <h3 class="subHead"><?php echo __('topics'); ?></h3>
    
    <?php
    //wątki
    if ($numTopics > 0){
    ?>
	<?php
	if ($msgTopic){
	    echo $message;
	}
	?>
	<div class="tableTop"></div>
	<table class="tableTopics">
	    <thead class="tableHead">
		<tr>
		<th><?php echo __('title'); ?></th><th class="topics"><?php echo __('topics'); ?></th><th class="responds"><?php echo __('posts'); ?></th>
		</tr>
	    </thead>

	<tbody>
	<?php
	foreach ($arrTopics as $value){
	    echo '<tr>';
	    echo '<td><a href="forum,s,' . $value['id_topic'] . ',' . trans_url_name($value['topic']) . '" class="topicLink">' . $value['topic'] . '</a><p>' . $value['content'] . '</p></td>';
	    echo '<td class="topics">' . $value['numTopics'] . '</td>';
	    echo '<td class="responds">' . $value['numPosts'] . '</td>';
	    echo '</tr>';
	}
	?>
	</tbody>
	
	</table>
	<div class="tableBottom"></div>
    <?php
    } else {
	echo '<p><b>' . __('no topics') . '</b></p>';
    }
    if ($addTopic){
	if ($topicId == 0){
	    echo '<a href="forum,d,' . $topicId . ',' . $urlAdd . '#watek" class="button">' . __('add new main topic') . '</a>';
	} else {
	    echo '<a href="forum,d,' . $topicId . ',' . $urlAdd . '#watek" class="button">' . __('add new current topic') . '</a>';
	}
    }
    ?>
        
    </div>
    
    <?php
    if ($topicId != 0){
    ?>
    
    <div class="forum-answers-wrapper">
    
        <a name="odpowiedzi" id="odpowiedzi"></a>
        <h3 class="subHead"><?php echo __('posts'); ?></h3>    

        <?php
        //posty
        if ($numPosts > 0 ){
        ?>
            <?php
            if ($msgPost){
                echo $message;
            }
            ?>    

            <ul class="listPosts">
            <?php
            $n = 1;
            foreach ($arrPosts as $value){

                echo '<li>';
                echo '<div class="postText clearfix">';

                echo '<span class="postDate">' . $value['date'] . '</span>';
                echo '<span class="postHour">' . $value['hour'] . '</span>';

                echo '<div class="postAuthor">';

                echo '<img class="forumAvatarImage" src="' . $value['avatar'] . '" alt="' . $value['alt'] . '" />';	    

                echo '<span class="author">' . $value['author'] . '</span>';
                echo '</div>';

                echo '<div class="postContent">' . nl2br($value['content']) . '</div>';
                echo '</div>';
                echo '<ul class="respondButtonList clearfix">';
                echo '<li><a href="forum,o,' . $topicId . ',0,' . $urlAddPost . '#odpowiedz" class="button">' . __('respond action') . '</a></li>';
                echo '<li><a href="forum,o,' . $topicId . ',' . $value['id_posts'] . ',' . $urlAddCita . '#odpowiedz" class="button">' . __('cite and respond action') . '</a></li>';
                echo '</ul>';
                echo '</li>';
                $n++;

            }
            ?>
            </ul>

        <?php
        } else {
            echo '<p><b>' . __('no posts') . '</b></p>';
        }

        ?>

    </div>
    
    <?php
    }
}
?>
</div>