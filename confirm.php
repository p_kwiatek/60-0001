<h2 class="content-header"><span><?php echo $pageName; ?></span></h2>
<div class="main-text">
    <?php echo $message; ?>
</div>

<div class="row">
    <ul class="list-unstyled list-inline col-xs-12 back-links">
        <li><a href="forum" class="button"><?php echo __('forum home page') ?></a></li>
        <li><a href="index.php" class="button"><?php echo __('home page') ?></a></li>
    </ul>
    <div class="clearfix"></div>
</div>